﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace q2gen.objects
{
    static class MapMessage
    {
        public static string Message { get; private set; }

        public static void AutoGenerateMessage()
        {
            Message = "Map generator (ver. 0) \n\n" +
                "by Mako\n\n" +
                "seed: " + Props.seed.ToString();
        }

        public static void NewMessage(string message)
        {
            Message = message;
        }

        public static void Append(string message)
        {
            Message += message;
        }

        public static string ToString()
        {
            string s = "";

            s += "\"message\" ";
            s += "\"" + Message + "\"\n";

            return s;
        }
    }
}
