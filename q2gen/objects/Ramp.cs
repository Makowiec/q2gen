﻿using System.Numerics;

namespace q2gen.objects
{
    class Ramp : Box
    {
        public Ramp(Vector3 center, Vector3 extents) : this(center, extents, new Texture()) { }

        public Ramp(Vector3 center, Vector3 extents, Texture texture)
        {
            bounds = new Bounds(center, extents);

            Vector3 mins = center - extents;
            Vector3 maxs = center + extents;

            Vector3 bottomBackLeft = mins;
            Vector3 bottomFrontLeft = new Vector3(mins.X, mins.Y, maxs.Z);
            Vector3 bottomFrontRight = new Vector3(maxs.X, mins.Y, maxs.Z);
            Vector3 bottomBackRight = new Vector3(maxs.X, mins.Y, mins.Z);

            Vector3 upBackLeft = new Vector3(mins.X, maxs.Y, mins.Z);
            Vector3 upFrontLeft = new Vector3(mins.X, maxs.Y, maxs.Z);
            Vector3 upFrontRight = maxs;
            Vector3 upBackRight = new Vector3(maxs.X, maxs.Y, mins.Z);

            bottom = new MyPlane(bottomBackLeft, bottomBackRight, bottomFrontRight, new Vector3(0, -1, 0), texture);
            up = new MyPlane(upBackLeft, upFrontLeft, upFrontRight, new Vector3(0, 1, 0), texture);
            left = new MyPlane(bottomFrontLeft, upFrontLeft, upBackLeft, new Vector3(-1, 0, 0), texture);
            right = new MyPlane(bottomBackRight, upBackRight, upFrontRight, new Vector3(1, 0, 0), texture);
            front = new MyPlane(bottomFrontRight, upFrontRight, upFrontLeft, new Vector3(0, 0, 1), texture);
            back = new MyPlane(bottomBackLeft, upFrontLeft, upFrontRight, texture);

            FitTexture(TextureFit.stretch);
        }
    }
}
