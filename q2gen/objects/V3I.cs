﻿using System;
using System.Numerics;

namespace q2gen.objects
{
    class V3I
    {
        public int x, y, z;

        public static V3I zero { get { return new V3I(); } }
        public static V3I right { get { return new V3I(1, 0, 0); } }
        public static V3I left { get { return new V3I(-1, 0, 0); } }
        public static V3I forward { get { return new V3I(0, 0, 1); } }
        public static V3I back { get { return new V3I(0, 0, -1); } }
        public static V3I up { get { return new V3I(0, 1, 0); } }
        public static V3I down { get { return new V3I(0, -1, 0); } }

        public static V3I operator +(V3I first, V3I second)
        {
            return new V3I(first.x + second.x, first.y + second.y, first.z + second.z);
        }

        public static V3I operator +(V3I vector, int value)
        {
            return new V3I(vector.x + value, vector.y + value, vector.z + value);
        }

        public static V3I operator -(V3I first, V3I second)
        {
            return new V3I(first.x - second.x, first.y - second.y, first.z - second.z);
        }

        public static V3I operator *(V3I vector, int value)
        {
            return new V3I(vector.x * value, vector.y * value, vector.z * value);
        }

        public static V3I operator /(V3I vector, int value)
        {
            return new V3I(vector.x / value, vector.y / value, vector.z / value);
        }

        public static bool operator ==(V3I first, V3I second)
        {
            return first.x == second.x && first.y == second.y && first.z == second.z;
        }

        public static bool operator !=(V3I first, V3I second)
        {
            return !(first.x == second.x && first.y == second.y && first.z == second.z);
        }

        public override string ToString()
        {
            return "(" + x.ToString() + ", " + y.ToString() + ", " + z.ToString() + ")";
        }

        public Vector3 ToVector3()
        {
            return new Vector3(x, y, z);
        }

        public static V3I Abs(V3I vector)
        {
            return new V3I(Math.Abs(vector.x), Math.Abs(vector.y), Math.Abs(vector.z));
        }

        public V3I() : this(0, 0, 0) { }

        public V3I(int value) : this(value, value, value) { }

        public V3I(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }
}
