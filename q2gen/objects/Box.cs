﻿using System.Collections.Generic;
using System.Numerics;

namespace q2gen.objects
{
    class Box : Brush
    {
        public MyPlane[] planes { get; set; } = new MyPlane[6];

        //right goes towards positive X, up towards positive Y, front towards positive Z
        public MyPlane bottom
        {
            get
            {
                return planes[0];
            }
            set
            {
                planes[0] = value;
            }
        }
        public MyPlane up
        {
            get
            {
                return planes[1];
            }
            set
            {
                planes[1] = value;
            }
        }
        public MyPlane left
        {
            get
            {
                return planes[2];
            }
            set
            {
                planes[2] = value;
            }
        }
        public MyPlane right
        {
            get
            {
                return planes[3];
            }
            set
            {
                planes[3] = value;
            }
        }
        public MyPlane front
        {
            get
            {
                return planes[4];
            }
            set
            {
                planes[4] = value;
            }
        }
        public MyPlane back
        {
            get
            {
                return planes[5];
            }
            set
            {
                planes[5] = value;
            }
        }

        public Vector3 center
        {
            get
            {
                return bounds.center;
            }
        }
        public Vector3 extents
        {
            get
            {
                return bounds.extents;
            }
        }
        public int width
        {
            get
            {
                return bounds.width;
            }
        }
        public int depth
        {
            get
            {
                return bounds.depth;
            }
        }
        public int height
        {
            get
            {
                return bounds.height;
            }
        }

        public void SetSurfaces(Surf surf)
        {
            foreach(MyPlane p in planes)
            {
                p.surface = surf;
            }
        }

        public void SetContents(Contents contents)
        {
            foreach (MyPlane p in planes)
            {
                p.contents = contents;
            }
        }

        public void SetValues(int value)
        {
            foreach (MyPlane p in planes)
            {
                p.value = value;
            }
        }

        public void AddContents(Contents contents)
        {
            foreach (MyPlane p in planes)
            {
                p.contents |= contents;
            }
        }

        public void AddSurfaceContents(Surf surf)
        {
            foreach (MyPlane p in planes)
            {
                p.surface |= surf;
            }
        }

        public void RemoveContents(Contents contents)
        {
            foreach (MyPlane p in planes)
            {
                p.contents &= ~contents;
            }
        }

        public void RemoveSurfaceContents(Surf surf)
        {
            foreach (MyPlane p in planes)
            {
                p.surface &= ~surf;
            }
        }

        public void SetBounds(Vector3 center, Vector3 extents)
        {
            bounds = new Bounds(center, extents);
        }

        public List<Vector3> GetAllPoints()
        {
            List<Vector3> pts = new List<Vector3>();

            foreach(MyPlane p in planes)
            {
                if (!pts.Contains(p.p1)) { pts.Add(p.p1); }
                if (!pts.Contains(p.p2)) { pts.Add(p.p2); }
                if (!pts.Contains(p.p3)) { pts.Add(p.p3); }
            }

            return pts;
        }

        public void FitTexture(TextureFit fit)
        {
            foreach(MyPlane p in planes)
            {
                p.FitTexture(fit);
            }
        }

        public override string ToString()
        {
            string s = "";

            s += "{\n";

            foreach(MyPlane p in planes)
            {
                if (p.ignorePlane) { continue; }
                s += p.ToString();
            }

            s += "}\n";

            return s;
        }

        public Box(Vector3 center, Vector3 extents) : this(center, extents, new Texture()) { }

        public Box(Vector3 center, Vector3 extents, Texture texture)
        {
            bounds = new Bounds(center, extents);

            Vector3 mins = center - extents;
            Vector3 maxs = center + extents;

            Vector3 bottomBackLeft = mins;
            Vector3 bottomFrontLeft = new Vector3(mins.X, mins.Y, maxs.Z);
            Vector3 bottomFrontRight = new Vector3(maxs.X, mins.Y, maxs.Z);
            Vector3 bottomBackRight = new Vector3(maxs.X, mins.Y, mins.Z);

            Vector3 upBackLeft = new Vector3(mins.X, maxs.Y, mins.Z);
            Vector3 upFrontLeft = new Vector3(mins.X, maxs.Y, maxs.Z);
            Vector3 upFrontRight = maxs;
            Vector3 upBackRight = new Vector3(maxs.X, maxs.Y, mins.Z);

            bottom = new MyPlane(bottomBackLeft, bottomBackRight, bottomFrontRight, new Vector3(0, -1, 0), texture);
            up = new MyPlane(upBackLeft, upFrontLeft, upFrontRight, new Vector3(0, 1, 0), texture);
            left = new MyPlane(bottomFrontLeft, upFrontLeft, upBackLeft, new Vector3(-1, 0, 0), texture);
            right = new MyPlane(bottomBackRight, upBackRight, upFrontRight, new Vector3(1, 0, 0), texture);
            front = new MyPlane(bottomFrontRight, upFrontRight, upFrontLeft, new Vector3(0, 0, 1), texture);
            back = new MyPlane(bottomBackLeft, upBackLeft, upBackRight, new Vector3(0, 0, -1), texture);

            FitTexture(TextureFit.closeFit);
        }

        public Box(Vector3 center, Vector3 extents, Texture texture, bool autofit)
        {
            bounds = new Bounds(center, extents);

            Vector3 mins = center - extents;
            Vector3 maxs = center + extents;

            Vector3 bottomBackLeft = mins;
            Vector3 bottomFrontLeft = new Vector3(mins.X, mins.Y, maxs.Z);
            Vector3 bottomFrontRight = new Vector3(maxs.X, mins.Y, maxs.Z);
            Vector3 bottomBackRight = new Vector3(maxs.X, mins.Y, mins.Z);

            Vector3 upBackLeft = new Vector3(mins.X, maxs.Y, mins.Z);
            Vector3 upFrontLeft = new Vector3(mins.X, maxs.Y, maxs.Z);
            Vector3 upFrontRight = maxs;
            Vector3 upBackRight = new Vector3(maxs.X, maxs.Y, mins.Z);

            bottom = new MyPlane(bottomBackLeft, bottomBackRight, bottomFrontRight, new Vector3(0, -1, 0), texture);
            up = new MyPlane(upBackLeft, upFrontLeft, upFrontRight, new Vector3(0, 1, 0), texture);
            left = new MyPlane(bottomFrontLeft, upFrontLeft, upBackLeft, new Vector3(-1, 0, 0), texture);
            right = new MyPlane(bottomBackRight, upBackRight, upFrontRight, new Vector3(1, 0, 0), texture);
            front = new MyPlane(bottomFrontRight, upFrontRight, upFrontLeft, new Vector3(0, 0, 1), texture);
            back = new MyPlane(bottomBackLeft, upBackLeft, upBackRight, new Vector3(0, 0, -1), texture);

            if (autofit)
            {
                FitTexture(TextureFit.closeFit);
            }
        }

        public Box(Texture texture, Vector3 mins, Vector3 maxs)
        {
            Vector3 bottomBackLeft = mins;
            Vector3 bottomFrontLeft = new Vector3(mins.X, mins.Y, maxs.Z);
            Vector3 bottomFrontRight = new Vector3(maxs.X, mins.Y, maxs.Z);
            Vector3 bottomBackRight = new Vector3(maxs.X, mins.Y, mins.Z);

            Vector3 upBackLeft = new Vector3(mins.X, maxs.Y, mins.Z);
            Vector3 upFrontLeft = new Vector3(mins.X, maxs.Y, maxs.Z);
            Vector3 upFrontRight = maxs;
            Vector3 upBackRight = new Vector3(maxs.X, maxs.Y, mins.Z);

            bottom = new MyPlane(bottomBackLeft, bottomBackRight, bottomFrontRight, new Vector3(0, -1, 0), texture);
            up = new MyPlane(upBackLeft, upFrontLeft, upFrontRight, new Vector3(0, 1, 0), texture);
            left = new MyPlane(bottomFrontLeft, upFrontLeft, upBackLeft, new Vector3(-1, 0, 0), texture);
            right = new MyPlane(bottomBackRight, upBackRight, upFrontRight, new Vector3(1, 0, 0), texture);
            front = new MyPlane(bottomFrontRight, upFrontRight, upFrontLeft, new Vector3(0, 0, 1), texture);
            back = new MyPlane(bottomBackLeft, upBackLeft, upBackRight, new Vector3(0, 0, -1), texture);

            FitTexture(TextureFit.closeFit);
        }
        
        public Box(Vector3 bottomBackLeft, Vector3 bottomFrontLeft, Vector3 bottomFrontRight, Vector3 bottomBackRight, 
            Vector3 upBackLeft, Vector3 upFrontLeft, Vector3 upFrontRight, Vector3 upBackRight, Texture texture)
        {
            bottom = new MyPlane(bottomBackLeft, bottomBackRight, bottomFrontRight, texture);
            up = new MyPlane(upBackLeft, upFrontLeft, upFrontRight, texture);
            left = new MyPlane(bottomFrontLeft, upFrontLeft, upBackLeft, texture);
            right = new MyPlane(bottomBackRight, upBackRight, upFrontRight, texture);
            front = new MyPlane(bottomFrontRight, upFrontRight, upFrontLeft, texture);
            back = new MyPlane(bottomBackLeft, upBackLeft, upBackRight, texture);

            FitTexture(TextureFit.closeFit);
        }

        public Box() { }
    }
}
