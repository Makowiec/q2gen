﻿using System.Numerics;
using System.Globalization;
using System;
using System.Collections.Generic;
using q2gen.generation;
using q2gen.utils;
using System.Linq;

namespace q2gen.objects
{
    class MyPlane
    {
        public bool ignorePlane = false;

        public Vector3 p1;
        public Vector3 p2;
        public Vector3 p3;

        private Texture texture;
        public Texture Texture
        {
            get
            {
                return texture;
            }
            set
            {
                texture = Texture.Clone(value);
            }
        }

        public Surf surface;
        public Contents contents;
        public int value;

        public Vector3 normal
        {
            get
            {
                return Vector3.Normalize(Vector3.Cross(p2 - p1, p3 - p1));
            }
        }

        public void SetTextureOffset(float x, float y)
        {
            texture.xOffset = x;
            texture.yOffset = y;
        }

        public void FitTexture(TextureFit fit)
        {
            if (fit == TextureFit.tile)
            {
                //reset texture to default state
                texture = new Texture(texture.TexName);
                return;
            }

            //Create texture space mins and maxs from points.
            //This is only accurate for axis aligned planes.
            //Rectangular ramps are mostly okay, more complicated
            //or rotated shapes not so much.
            Vector2 mins = new Vector2();
            Vector2 maxs = new Vector2();
            float width = 0;
            float height = 0;

            //We project the face on an axis avoiding deformation as
            //much as possible. This seems accurate enough.
            float dotx = Vector3.Dot(normal, Vector3.UnitX);
            float doty = Vector3.Dot(normal, Vector3.UnitY);
            float dotz = Vector3.Dot(normal, Vector3.UnitZ);

            int sign = 1;
            Axis axis;
            if (dotx <= 0 && doty <= 0 && dotz <= 0)
            {
                if(doty <= dotx && doty <= dotz)
                {
                    //Plane is facing down with angle lesser or equal to 45 deg.
                    axis = Axis.y;
                }
                else if(dotx < dotz)
                {
                    //Plane is facing left.
                    axis = Axis.x;
                }
                else
                {
                    //Plane is facing back.
                    axis = Axis.z;
                }
            }
            else
            {
                if (doty > dotx && doty > dotz)
                {
                    //Plane is facing up: floor or a flat ramp (less than 45 deg).
                    axis = Axis.y;
                }
                else if (dotx > dotz)
                {
                    //Plane is facing right.
                    axis = Axis.x;
                }
                else
                {
                    //Plane is facing forward.
                    axis = Axis.z;
                }
            }

            //Projecting face onto the correct axis.
            switch (axis)
            {
                case Axis.x:
                    mins = new Vector2(Tools.Min3(p1.Z, p2.Z, p3.Z), Tools.Min3(p1.Y, p2.Y, p3.Y));
                    maxs = new Vector2(Tools.Max3(p1.Z, p2.Z, p3.Z), Tools.Max3(p1.Y, p2.Y, p3.Y));
                    sign = mins.X > 0 ? -1 : 1; //not sure why I did it like this...
                    break;
                case Axis.y:
                    mins = new Vector2(Tools.Min3(p1.X, p2.X, p3.X), Tools.Min3(p1.Z, p2.Z, p3.Z));
                    maxs = new Vector2(Tools.Max3(p1.X, p2.X, p3.X), Tools.Max3(p1.Z, p2.Z, p3.Z));
                    sign = mins.X > 0 ? -1 : 1;
                    break;
                case Axis.z:
                    mins = new Vector2(Tools.Min3(p1.X, p2.X, p3.X), Tools.Min3(p1.Y, p2.Y, p3.Y));
                    maxs = new Vector2(Tools.Max3(p1.X, p2.X, p3.X), Tools.Max3(p1.Y, p2.Y, p3.Y));
                    sign = mins.X > 0 ? -1 : 1;
                    break;
            }

            //calculate local width and height of the plane
            width = Math.Abs(maxs.X - mins.X);
            height = Math.Abs(maxs.Y - mins.Y);

            //stretch texture to fit the face
            if(fit == TextureFit.stretch)
            {
                //set texture scale
                texture.xScale = width / texture.xDimension;
                texture.yScale = height / texture.yDimension;

                //set texture offset
                texture.xOffset = Math.Abs(mins.X / texture.xScale) * sign;
                texture.yOffset = mins.Y / texture.yScale;
            }
            //fit as many textures as possible into a face and stretch them to fit the face
            else if (fit == TextureFit.closeFit)
            {
                float xScale = width / texture.xDimension;
                float yScale = height / texture.yDimension;

                //How many textures does fit in that face?
                int countX = (int)Math.Floor(width / texture.xDimension);
                int countY = (int)Math.Floor(height / texture.yDimension);

                //How much space do they occupy?
                float xl = countX * texture.xDimension;
                float yl = countY * texture.yDimension;

                if(xl == 0)
                {
                    //Didn't fit a single texture in the x axis.
                    texture.xScale = width / texture.xDimension;
                    float yd = texture.yDimension * texture.xScale;
                    yl = (int)Math.Floor(height / yd) * yd;
                    texture.yScale = height / yl;
                    texture.yScale *= texture.xScale;
                }
                else if(yl == 0)
                {
                    //Didn't fit a single texture in the y axis.
                    texture.yScale = height / texture.yDimension;
                    float xd = texture.xDimension * texture.xScale;
                    xl = (int)Math.Floor(width / xd) * xd;
                    texture.xScale = width / xl;
                    texture.xScale *= texture.yScale;
                }
                else
                {
                    texture.xScale = width / xl;
                    texture.yScale = height / yl;
                }

                texture.xOffset = Math.Abs(mins.X / texture.xScale) * sign;
                texture.yOffset = mins.Y / texture.yScale;
            }
        }

        public override string ToString()
        {
            string s = "";

            s += "( " + p1.X.ToString() + " " + p1.Z.ToString() + " " + p1.Y.ToString() + " ) ";
            s += "( " + p2.X.ToString() + " " + p2.Z.ToString() + " " + p2.Y.ToString() + " ) ";
            s += "( " + p3.X.ToString() + " " + p3.Z.ToString() + " " + p3.Y.ToString() + " ) ";
            s += Texture.TexName + " " + Texture.xOffset.ToString(CultureInfo.InvariantCulture) + " " + Texture.yOffset.ToString(CultureInfo.InvariantCulture) 
                + " " + Texture.rotation.ToString(CultureInfo.InvariantCulture) + " "
                + Texture.xScale.ToString(CultureInfo.InvariantCulture) + " " + Texture.yScale.ToString(CultureInfo.InvariantCulture)
                + (contents != 0 || surface != 0 || value != 0 ? " " + ((int)contents).ToString() + " " + ((int)surface).ToString() + " " + value.ToString() + " " : "") + "\n";

            return s;
        }

        public MyPlane(Vector3 p1, Vector3 p2, Vector3 p3) : this(p1, p2, p3, new Texture()) { }

        public MyPlane(Vector3 p1, Vector3 p2, Vector3 p3, Texture texture)
        {
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.Texture = texture;
        }

        public MyPlane(Vector3 p1, Vector3 p2, Vector3 p3, Vector3 normal) : this(p1, p2, p3, normal, new Texture()) { }

        public MyPlane(Vector3 p1, Vector3 p2, Vector3 p3, Vector3 normal, Texture texture)
        {
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;

            if (this.normal != normal)
            {
                this.p1 = p3;
                this.p2 = p2;
                this.p3 = p1;
            }

            this.Texture = texture;
        }

        public MyPlane(Vector3 p1, Vector3 p2, Vector3 p3, Texture texture, Surf surface, Contents contents, int value) : this(p1, p2, p3, texture)
        {
            this.surface = surface;
            this.contents = contents;
            this.value = value;
        }
    }
}
