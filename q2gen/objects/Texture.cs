﻿using q2gen.IO;
using System;

namespace q2gen.objects
{
    class Texture
    {
        private string texName;
        public string TexName
        {
            get
            {
                return texName;
            }
            set
            {
                texName = value;
                ReadDimensions();
            }
        }
        public float xOffset { get; set; }
        public float yOffset { get; set; }
        public float rotation { get; set; }
        public float xScale { get; set; }
        public float yScale { get; set; }

        public int xDimension { get; set; }
        public int yDimension { get; set; }

        public static Texture Clone(Texture texture)
        {
            return new Texture(texture.TexName, texture.xOffset, texture.yOffset, texture.rotation, texture.xScale, texture.yScale, texture.xDimension, texture.yDimension);
        }

        private void ReadDimensions()
        {
            string path = Props.texturePath + TexName.Replace('/', '\\') + ".wal";

            int[] dimensions = QWalReader.GetWalDimensions(path);

            xDimension = dimensions[0];
            yDimension = dimensions[1];
        }

        public Texture() : this("e1u1/metal14_1") { }

        public Texture(string name) : this(name, 0, 0, 0, 1, 1) { }

        public Texture(string name, float xOffset, float yOffset, float rotation, float xScale, float yScale)
        {
            texName = name;
            this.xOffset = xOffset;
            this.yOffset = yOffset;
            this.rotation = rotation;
            this.xScale = xScale;
            this.yScale = yScale;

            ReadDimensions();
        }

        public Texture(string name, float xOffset, float yOffset, float rotation, float xScale, float yScale, int xDimension, int yDimension)
        {
            texName = name;
            this.xOffset = xOffset;
            this.yOffset = yOffset;
            this.rotation = rotation;
            this.xScale = xScale;
            this.yScale = yScale;
            this.xDimension = xDimension;
            this.yDimension = yDimension;
        }
    }
}
