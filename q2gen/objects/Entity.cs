﻿using System.Collections.Generic;
using System.Numerics;

namespace q2gen.objects
{
    class Entity
    {
        public string classname { get; set; }
        public Vector3 origin { get; set; }
        public int spawnflags { get; set; } = 0;

        public Dictionary<string, string> properties = new Dictionary<string, string>();

        public override string ToString()
        {
            string s = "{\n";

            s += "\"classname\"" + " \"" + classname + "\"\n";
            s += "\"origin\"" + " \"" + origin.X.ToString() + " " + origin.Z.ToString() + " " + origin.Y.ToString() + "\"\n";
            s += "\"spawnflags\"" + " \"" + spawnflags.ToString() + "\"\n";

            foreach (var p in properties)
            {
                s += "\"" + p.Key + "\"" + " \"" + p.Value + "\"\n";
            }
            s += "}\n";

            return s;
        }

        public Entity(string classname, Vector3 origin, int spawnflags)
        {
            this.classname = classname;
            this.origin = origin;
            this.spawnflags = spawnflags;
        }

        public Entity(string classname, Vector3 origin)
        {
            this.classname = classname;
            this.origin = origin;
        }
    }
}
