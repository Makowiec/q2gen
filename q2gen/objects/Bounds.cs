﻿using System.Numerics;

namespace q2gen.objects
{

    struct Bounds
    {
        public Vector3 center { get; set; }
        public Vector3 extents { get; set; }
        public Vector3 mins { get { return center - extents; } }
        public Vector3 maxs { get { return center + extents; } }
        public Vector3 size { get { return extents * 2; } }
        public int width { get { return (int)size.X; } }
        public int height { get { return (int)size.Y; } }
        public int depth { get { return (int)size.Z; } }

        public Bounds(Vector3 center, Vector3 extents) : this()
        {
            this.center = center;
            this.extents = extents;
        }
    }
}