﻿using System.Collections.Generic;
using System.IO;
using q2gen.objects;

namespace q2gen
{
    static class Style
    {
        private static List<Texture> textures = new List<Texture>();

        public static Texture wallTexture { get; set; }
        public static Texture floorTexture { get; set; }
        public static Texture ceilingTexture { get; set; }
        public static Texture blockTexture { get; set; }
        public static Texture smallBlockTexture { get; set; }
        public static Texture detailTexture { get; set; }
        public static Texture ceilingLightTexture { get; set; }
        public static Texture clip { get; set; }
        public static Texture ice { get; set; }
        public static Texture gizmo { get; set; }
        public static Texture smallLight { get; set; }
        public static Texture ladderSteps { get; set; }
        public static Texture ladderBars { get; set; }

        public static string GetFilelist()
        {
            string s = "";

            foreach(Texture t in textures)
            {
                string path = Props.texturePath + t.TexName;
                if (File.Exists(path + ".jpg"))
                {
                    s += "textures/" + t.TexName + ".jpg\n";
                }
                else if (File.Exists(path + ".png"))
                {
                    s += "textures/" + t.TexName + ".png\n";
                }
            }

            return s;
        }

        static Style()
        {
            wallTexture = new Texture("_adrom_colors/drom_wl05_white");
            floorTexture = new Texture("_adrom/drom_wl44");
            blockTexture = new Texture("_adrom/drom_wl25");
            smallBlockTexture = new Texture("_adrom_colors/drom_wl23_yel");
            detailTexture = new Texture("_adrom/drom_wl32");
            ceilingTexture = new Texture("_adrom/drom_wl31");
            ceilingLightTexture = new Texture("_adrom_colors/drom_li12_yel");
            clip = new Texture("_aceflags/flag_clip");
            ice = new Texture("_adrom_colors/drom_wl05_yel");
            gizmo = new Texture("_adrom_colors/drom_wl05_green");
            smallLight = new Texture("_adrom/drom_li10");
            ladderSteps = new Texture("_adrom/drom_wl42");
            ladderBars = new Texture("_adrom/drom_li22");

            textures.Add(wallTexture);
            textures.Add(floorTexture);
            textures.Add(blockTexture);
            textures.Add(smallBlockTexture);
            textures.Add(detailTexture);
            textures.Add(ceilingTexture);
            textures.Add(ceilingLightTexture);
            textures.Add(clip);
            textures.Add(ice);
            textures.Add(gizmo);
            textures.Add(smallLight);
            textures.Add(ladderSteps);
            textures.Add(ladderBars);
        }
    }
}
