﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace q2gen
{
    static class Props
    {
        public static CompilerType compilerType = CompilerType.dll;
        public static bool compileOnly = false;

        //file paths
        public static string mapPath = "G:\\BSP97\\Quake2\\maps\\generated.map";
        public static string mapFolder = "G:\\BSP97\\Quake2\\maps\\";
        public static string compiledMapPath = "G:\\BSP97\\Quake2\\maps\\generated.bsp";
        public static string gameDir = "C:\\quake2\\baseq2";
        public static string modDir = "C:\\Users\\Maciek\\AppData\\Local\\.q2online\\jumptest";
        public static string gamePath = "C:\\Users\\Maciek\\AppData\\Local\\.q2online\\q2proclean.exe";
        public static string mapFolderPath = "C:\\Users\\Maciek\\AppData\\Local\\.q2online\\jumptest\\maps\\";
        public static string mapName = "generated";
        public static string texturePath = "C:\\Users\\Maciek\\AppData\\Local\\.q2online\\jumptest\\textures\\";

        //generator settings
        public static readonly int blockSize = 320;                  //multiplications of 64 are recommended
        public static readonly int wallThickness = 64;               //should be divisible by two
        public static readonly int roomSize = 3;                     //real room size = roomSize * blockSize
        public static readonly int maximumAttempts = 4096;           //trying forever will most likely end up with an infinite loop
        public static readonly int tunnelSegments = 2000;            //this includes start, end, tunnels, turns and gates only
        public static readonly float roomSpawnChance = 0.1f;         //chance that we try to spawn a room when we get two blocks going in the same direction
        public static readonly float followDirectionChance = 0.7f;   //typical chance to follow last block direction

        public static readonly int worldSize = 8192;                 //this is defined by Q2 engine and cannot be altered

        public static Random random = new Random();
        public static int seed = 0;

        public static List<int[]> multiHeightTables = new List<int[]>
        {
            new int[] { 128, 256 },
            new int[] { 128, 192 },
            new int[] { 128, 240 },
            new int[] { 128, 256, 320 },
            new int[] { 128, 248, 320 },
            new int[] { 128, 256, 320 },
            new int[] { 128, 248, 320 },
            new int[] { 160, 240 },
            new int[] { 144, 240 }
        };

        public static int MaxMultiHeight
        {
            get
            {
                var val = multiHeightTables.OrderByDescending(i => i[i.Length - 1]).First();
                return val[val.Length - 1];
            }
        }

        public static T RandomEnumValue<T>()
        {
            var v = Enum.GetValues(typeof(T));
            return (T)v.GetValue(random.Next(v.Length));
        }

        public static float RandomFloat()
        {
            return (float)random.NextDouble();
        }
    }
}
