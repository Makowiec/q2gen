﻿using System.Numerics;
using q2gen.objects;
using System;
using System.Collections.Generic;

namespace q2gen.utils
{
    class Tools
    {
        public static Direction NegativeDirection(Direction source)
        {
            switch (source)
            {
                case Direction.back:
                    return Direction.forward;
                case Direction.forward:
                    return Direction.back;
                case Direction.left:
                    return Direction.right;
                case Direction.right:
                    return Direction.left;
                case Direction.up:
                    return Direction.down;
                default:
                    return Direction.up;
            }
        }

        public static int DirectionToAngle(Direction source)
        {
            switch (source)
            {
                case Direction.back:
                    return 270;
                case Direction.forward:
                    return 90;
                case Direction.left:
                    return 180;
                case Direction.right:
                    return 0;
                case Direction.up:
                    return -1;
                default:
                    return -2;
            }
        }

        public static Axis AxisFromDirection(Direction source)
        {
            switch (source)
            {
                case Direction.back:
                case Direction.forward:
                    return Axis.z;
                case Direction.left:
                case Direction.right:
                    return Axis.x;
                default:
                    return Axis.y;
            }
        }

        /// <returns>Array of alignments that touch the bounds in specified direction.</returns>
        public static Alignment[] GetAdjacentAlignments(Direction direction)
        {
            Alignment[] alignments = new Alignment[3];

            switch (direction)
            {
                case Direction.back:
                    alignments[0] = Alignment.back;
                    alignments[1] = Alignment.backLeft;
                    alignments[2] = Alignment.backRight;
                    break;
                case Direction.forward:
                    alignments[0] = Alignment.front;
                    alignments[1] = Alignment.frontLeft;
                    alignments[2] = Alignment.frontRight;
                    break;
                case Direction.left:
                    alignments[0] = Alignment.left;
                    alignments[1] = Alignment.backLeft;
                    alignments[2] = Alignment.frontLeft;
                    break;
                case Direction.right:
                    alignments[0] = Alignment.right;
                    alignments[1] = Alignment.backRight;
                    alignments[2] = Alignment.frontRight;
                    break;
                default:
                    alignments[0] = Alignment.center;
                    alignments[1] = Alignment.center;
                    alignments[2] = Alignment.center;
                    break;
            }

            return alignments;
        }

        /// <returns>Centered alignment towards direction.</returns>
        public static Alignment DirectionToAlignment(Direction source)
        {
            switch (source)
            {
                case Direction.back:
                    return Alignment.back;
                case Direction.left:
                    return Alignment.left;
                case Direction.forward:
                    return Alignment.front;
                case Direction.right:
                    return Alignment.right;
                default:
                    return Alignment.center;
            }
        }

        public static Direction AlignmentToDirection(Alignment alignment)
        {
            switch (alignment)
            {
                case Alignment.back:
                case Alignment.backLeft:
                    return Direction.back;
                case Alignment.left:
                case Alignment.frontLeft:
                    return Direction.left;
                case Alignment.front:
                case Alignment.frontRight:
                    return Direction.forward;
                case Alignment.right:
                case Alignment.backRight:
                    return Direction.right;
                default:
                    return Direction.forward;
            }
        }

        /// <returns>Returns random alignment in specified direction.</returns>
        public static Alignment RandomAdjacentAlignment(Direction direction)
        {
            Alignment[] alignments = GetAdjacentAlignments(direction);

            return alignments[Props.random.Next(alignments.Length)];
        }

        /// <param name="source">Up and down should be only used with X and Z axis rotations.</param>
        /// <returns>Angle between forward axis and specified direction.</returns>
        public static int EulerAngleForwardToDirection(Direction source)
        {
            switch (source)
            {
                case Direction.back:
                    return 180;
                case Direction.forward:
                    return 0;
                case Direction.left:
                    return 90;
                case Direction.right:
                    return -90;
                case Direction.up:
                    return 90;
                default:
                    return -90;
            }
        }

        public static int EulerAngleForwardToAlignment(Alignment alignment)
        {
            switch (alignment)
            {
                case Alignment.back:
                    return 180;
                case Alignment.backLeft:
                    return Props.RandomFloat() > 0.5f ? 180 : 90;
                case Alignment.left:
                    return 90;
                case Alignment.frontLeft:
                    return Props.RandomFloat() > 0.5f ? 0 : 90;
                case Alignment.front:
                    return 0;
                case Alignment.frontRight:
                    return Props.RandomFloat() > 0.5f ? 0 : -90;
                case Alignment.right:
                    return -90;
                case Alignment.backRight:
                    return Props.RandomFloat() > 0.5f ? 180 : -90;
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Flips the plane direction.
        /// </summary>
        public static void FlipPlaneNormal(MyPlane plane)
        {
            Vector3 p1 = plane.p1;
            plane.p1 = plane.p3;
            plane.p3 = p1;
        }

        public static bool IsBoxOverlappingBounds(Box checkedBox, Bounds overlappedBounds)
        {
            Bounds cb = checkedBox.bounds;
            Bounds ob = overlappedBounds;

            return (cb.mins.X < ob.maxs.X && cb.maxs.X > ob.mins.X) &&
                   (cb.mins.Y < ob.maxs.Y && cb.maxs.Y > ob.mins.Y) &&
                   (cb.mins.Z < ob.maxs.Z && cb.maxs.Z > ob.mins.Z);
        }

        public static bool IsBoxOverlappingBox(Box checkedBox, Box overlappedBox)
        {
            Bounds cb = checkedBox.bounds;
            Bounds ob = overlappedBox.bounds;

            return (cb.mins.X < ob.maxs.X && cb.maxs.X > ob.mins.X) &&
                   (cb.mins.Y < ob.maxs.Y && cb.maxs.Y > ob.mins.Y) &&
                   (cb.mins.Z < ob.maxs.Z && cb.maxs.Z > ob.mins.Z);
        }

        public static bool IsBoxOverlappingStructure(Box checkedBox, List<Box> structure)
        {
            foreach(Box b in structure)
            {
                if(IsBoxOverlappingBox(checkedBox, b))
                {
                    return true;
                }
            }
            return false;
        }

        public static float Min3(float one, float two, float three)
        {
            return Math.Min(one, Math.Min(two, three));
        }

        public static float Max3(float one, float two, float three)
        {
            return Math.Max(one, Math.Max(two, three));
        }
    }
}
