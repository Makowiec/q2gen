﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace q2gen.utils
{
    static class Vector3Extensions
    {
        public static bool IsGreaterOrEqual(this Vector3 local, Vector3 other)
        {
            return local.X >= other.X && local.Y >= other.Y && local.Z >= other.Z;
        }

        public static bool IsLesserOrEqual(this Vector3 local, Vector3 other)
        {
            return local.X <= other.X && local.Y <= other.Y && local.Z <= other.Z;
        }
    }
}
