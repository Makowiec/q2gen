﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace q2gen
{
    static class ConsoleExt
    {
        public static void WarningMSG(string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Separator();
            Console.WriteLine("WARNING: " + message);
            Separator();
            Console.ResetColor();
            Empty();
        }

        public static void NotificationMSG(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        public static void HighMSG(string message)
        {
            
            Console.ForegroundColor = ConsoleColor.Cyan;
            Separator();
            Console.WriteLine(message);
            Separator();
            Console.ResetColor();
        }

        public static void HeaderMSG(string message)
        {
            NotificationMSG(message);
            Separator();
        }

        public static void Empty()
        {
            Console.WriteLine();
        }

        public static void ErrorMSG(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Separator();
            Console.WriteLine("ERROR: " + message);
            Separator();
            Console.ResetColor();
            Empty();
        }

        public static void MSG(string message)
        {
            Console.WriteLine(message);
        }

        public static void Separator()
        {
            Console.WriteLine("- - - - - - - - - - - - - - - -");
        }

        public static void TableEntry(string key, string value)
        {
            int length = key.Length + value.Length;

            Console.Write(key);
            for(int i = 0; i < 31 - length; i++)
            {
                Console.Write(" ");
            }
            Console.WriteLine(value);
        }

        public static void ColorMSG(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ResetColor();
        }
    }
}
