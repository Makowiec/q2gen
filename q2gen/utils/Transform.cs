﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

using q2gen.objects;

namespace q2gen.utils
{
    static class Transform
    {
        /// <summary>
        /// Translates box so its center moves by the offset.
        /// </summary>
        /// <param name="box">Box to be moved.</param>
        /// <param name="offset">Moves towards this vector.</param>
        public static void TranslateBox(Box box, Vector3 offset)
        {
            box.SetBounds(box.bounds.center + offset, box.bounds.extents);

            foreach(MyPlane plane in box.planes)
            {
                TranslatePlane(plane, offset);
            }
        }

        public static void TranslateBoxes(List<Box> boxes, Vector3 offset)
        {
            foreach(Box box in boxes)
            {
                TranslateBox(box, offset);
            }
        }

        /// <summary>
        /// Translates plane by the given offset.
        /// </summary>
        public static void TranslatePlane(MyPlane plane, Vector3 offset)
        {
            //translate every point by the given offset
            plane.p1 += offset;
            plane.p2 += offset;
            plane.p3 += offset;
        }
        
        /// <summary>
        /// Recalculates bounds for a box (for example after rotation). Keep in mind that the resulting bounds are axis aligned.
        /// </summary>
        /// <param name="box">Box to be recalculated.</param>
        public static void RecalculateBounds(Box box)
        {
            //6 queries is probably not the best solution to this problem
            var pts = box.GetAllPoints();

            float xMax = pts.OrderByDescending(i => i.X).First().X;
            float yMax = pts.OrderByDescending(i => i.Y).First().Y;
            float zMax = pts.OrderByDescending(i => i.Z).First().Z;

            float xMin = pts.OrderBy(i => i.X).First().X;
            float yMin = pts.OrderBy(i => i.Y).First().Y;
            float zMin = pts.OrderBy(i => i.Z).First().Z;

            Vector3 maxs = new Vector3(xMax, yMax, zMax);
            Vector3 mins = new Vector3(xMin, yMin, zMin);

            Vector3 extents = (maxs - mins) / 2;
            Vector3 center = maxs - extents;

            box.SetBounds(center, extents);
        }

        /// <summary>
        /// Allows to calculate bounds for a structure.
        /// </summary>
        /// <param name="boxes">Structure's boxes.</param>
        /// <returns>Smallest axis aligned bounds containing every box provided.</returns>
        public static Bounds GetStructureBounds(List<Box> boxes)
        {
            List<Vector3> pts = new List<Vector3>();

            foreach(Box b in boxes)
            {
                pts.AddRange(b.GetAllPoints());
            }

            float xMax = pts.OrderByDescending(i => i.X).First().X;
            float yMax = pts.OrderByDescending(i => i.Y).First().Y;
            float zMax = pts.OrderByDescending(i => i.Z).First().Z;

            float xMin = pts.OrderBy(i => i.X).First().X;
            float yMin = pts.OrderBy(i => i.Y).First().Y;
            float zMin = pts.OrderBy(i => i.Z).First().Z;

            Vector3 maxs = new Vector3(xMax, yMax, zMax);
            Vector3 mins = new Vector3(xMin, yMin, zMin);

            Vector3 extents = (maxs - mins) / 2;
            Vector3 center = maxs - extents;

            return new Bounds(center, extents);
        }

        /// <summary>
        /// Rotates box around specified pivot point (example: rotating room contents around its center).
        /// </summary>
        /// <param name="box">Box to be rotated.</param>
        /// <param name="axis">Axis to rotate around.</param>
        /// <param name="pivot">Box rotates around this point.</param>
        /// <param name="eulerAngle">Angle in degrees.</param>
        public static void RotateBoxAroundPivot(Box box, Axis axis, Vector3 pivot, int eulerAngle)
        {
            //bring object to pivot
            //Vector3 center = new Vector3(box.bounds.center.X, box.bounds.center.Y, box.bounds.center.Z);
            TranslateBox(box, -pivot);

            float cos = (float)Math.Cos(Rad(eulerAngle));
            float sin = (float)Math.Sin(Rad(eulerAngle));

            //rotate each point around origin
            switch (axis)
            {
                case Axis.x:
                    foreach (MyPlane p in box.planes)
                    {
                        //p1
                        float y, z;
                        y = p.p1.Y * cos - p.p1.Z * sin;
                        z = p.p1.Z * cos + p.p1.Y * sin;
                        p.p1 = new Vector3(p.p1.X, y, z);
                        //p2
                        y = p.p2.Y * cos - p.p2.Z * sin;
                        z = p.p2.Z * cos + p.p2.Y * sin;
                        p.p2 = new Vector3(p.p2.X, y, z);
                        //p3
                        y = p.p3.Y * cos - p.p3.Z * sin;
                        z = p.p3.Z * cos + p.p3.Y * sin;
                        p.p3 = new Vector3(p.p3.X, y, z);
                    }
                    break;
                case Axis.y:
                    foreach (MyPlane p in box.planes)
                    {
                        float x, z;
                        x = p.p1.X * cos - p.p1.Z * sin;
                        z = p.p1.Z * cos + p.p1.X * sin;
                        p.p1 = new Vector3(x, p.p1.Y, z);

                        x = p.p2.X * cos - p.p2.Z * sin;
                        z = p.p2.Z * cos + p.p2.X * sin;
                        p.p2 = new Vector3(x, p.p2.Y, z);

                        x = p.p3.X * cos - p.p3.Z * sin;
                        z = p.p3.Z * cos + p.p3.X * sin;
                        p.p3 = new Vector3(x, p.p3.Y, z);
                    }
                    break;
                case Axis.z:
                    foreach (MyPlane p in box.planes)
                    {
                        float x, y;
                        x = p.p1.X * cos - p.p1.Y * sin;
                        y = p.p1.Y * cos + p.p1.X * sin;
                        p.p1 = new Vector3(x, y, p.p1.Z);

                        x = p.p2.X * cos - p.p2.Y * sin;
                        y = p.p2.Y * cos + p.p2.X * sin;
                        p.p2 = new Vector3(x, y, p.p2.Z);

                        x = p.p3.X * cos - p.p3.Y * sin;
                        y = p.p3.Y * cos + p.p3.X * sin;
                        p.p3 = new Vector3(x, y, p.p3.Z);
                    }
                    break;
            }

            //move object back to where it was
            TranslateBox(box, pivot);
            RecalculateBounds(box);
        }

        public static void RotateBoxesAroundPivot(List<Box> boxes, Axis axis, Vector3 pivot, int eulerAngle)
        {
            foreach(Box b in boxes)
            {
                RotateBoxAroundPivot(b, axis, pivot, eulerAngle);
            }
        }

        /// <summary>
        /// Rotates box around its center (locally).
        /// </summary>
        /// <param name="box">Box to be rotated.</param>
        /// <param name="axis">Axis of rotation.</param>
        /// <param name="eulerAngle">Angle in degrees.</param>
        public static void RotateBox(Box box, Axis axis, int eulerAngle)
        {
            RotateBoxAroundPivot(box, axis, box.center, eulerAngle);
            return;
        }

        private static double Rad(int euler)
        {
            return (Math.PI / (double)180) * euler;
        }
    }
}
