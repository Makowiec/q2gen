﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;

namespace q2gen.compilation
{
    static class Compiler
    {
        [DllImport("qbsp3.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void RunQbsp(string args);

        [DllImport("qvis3.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void RunQvis(string args);

        [DllImport("qrad3.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void RunQrad(string args);

        static string qbspName = "qbsp3.exe";
        static string qvisName = "qvis3.exe";
        static string qradName = "arghrad.exe";
        static string quemapName = "quemap/quemap.exe";

        public static void CompileMap2()
        {
            if(Props.compilerType == CompilerType.none)
            {
                return;
            }
            ConsoleExt.HighMSG("Compiling map...");
            ConsoleExt.Empty();
            switch (Props.compilerType)
            {
                case CompilerType.shell:
                    ConsoleExt.WarningMSG("Shell compilers show no output until they're done working!");
                    ExecShellQbsp();
                    ConsoleExt.Separator();
                    ConsoleExt.Empty();
                    ExecShellQvis();
                    ConsoleExt.Separator();
                    ConsoleExt.Empty();
                    ExecShellQrad();
                    ConsoleExt.Empty();
                    break;
                case CompilerType.dll:
                    ExecDllQbsp();
                    ConsoleExt.Separator();
                    ConsoleExt.Empty();
                    ExecDllQvis();
                    ConsoleExt.Separator();
                    ConsoleExt.Empty();
                    ExecDllQrad();
                    ConsoleExt.Empty();
                    break;
                case CompilerType.quemap:
                    ExecQuemap();
                    ConsoleExt.Separator();
                    ConsoleExt.Empty();
                    break;
            }

            CopyFile();
        }

        private static void CaptureStandardOutput(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine("> " + e.Data);
        }

        public static void ExecQuemap()
        {
            ConsoleExt.HeaderMSG("Running quemap.");

            Process quemap = new Process();
            quemap.StartInfo.FileName = quemapName;
            quemap.StartInfo.Arguments = "-legacy -bsp -vis -light -patch 128 -extra -antialias -indirect " +
                "-path C:\\Users\\Maciek\\AppData\\Local\\.q2online\\jumptest -wpath G:\\BSP97\\Quake2 " + Props.mapPath;
            quemap.StartInfo.UseShellExecute = false;
            quemap.StartInfo.RedirectStandardOutput = true;
            quemap.OutputDataReceived += CaptureStandardOutput;
            quemap.Start();
            quemap.BeginOutputReadLine();

            quemap.WaitForExit();
        }

        public static void ExecShellQbsp()
        {
            ConsoleExt.HeaderMSG("Running qbsp.");

            Process qbsp = new Process();
            qbsp.StartInfo.FileName = qbspName;
            qbsp.StartInfo.Arguments = Props.mapPath;
            qbsp.StartInfo.UseShellExecute = false;
            qbsp.StartInfo.RedirectStandardOutput = true;
            qbsp.OutputDataReceived += CaptureStandardOutput;
            qbsp.Start();
            qbsp.BeginOutputReadLine();

            qbsp.WaitForExit();
        }

        public static void ExecDllQbsp()
        {
            ConsoleExt.HeaderMSG("Running qbsp from DLL.");

            string s = "qbsp3 " + Props.mapPath;
            RunQbsp(s);
        }

        public static void ExecShellQvis()
        {
            ConsoleExt.HeaderMSG("Running qvis.");

            Process qvis = new Process();
            qvis.StartInfo.FileName = qvisName;
            qvis.StartInfo.Arguments = Props.compiledMapPath;
            qvis.StartInfo.UseShellExecute = false;
            qvis.StartInfo.RedirectStandardOutput = true;
            qvis.OutputDataReceived += CaptureStandardOutput;
            qvis.Start();
            qvis.BeginOutputReadLine();

            qvis.WaitForExit();
        }

        public static void ExecDllQvis()
        {
            ConsoleExt.HeaderMSG("Running qvis from DLL.");

            string s = "qvis3 " + Props.compiledMapPath;
            RunQvis(s);
        }

        public static void ExecShellQrad()
        {
            ConsoleExt.HeaderMSG("Running qrad.");

            Process qrad = new Process();
            qrad.StartInfo.FileName = qradName;
            qrad.StartInfo.Arguments = "-v -gamedir " + Props.gameDir + " -moddir " + Props.modDir + " -chop 128 " + Props.compiledMapPath;
            qrad.StartInfo.UseShellExecute = false;
            qrad.StartInfo.RedirectStandardOutput = true;
            qrad.OutputDataReceived += CaptureStandardOutput;
            qrad.Start();
            qrad.BeginOutputReadLine();

            qrad.WaitForExit();
        }

        public static void ExecDllQrad()
        {
            ConsoleExt.HeaderMSG("Running qrad from DLL.");

            string s = "qrad3 " + "-gamedir " + Props.gameDir + " -moddir " + Props.modDir + " -chop 1024 -bounce 2 " + Props.compiledMapPath;
            RunQrad(s);
        }

        public static void CopyFile()
        {
            ConsoleExt.HeaderMSG("Copying file.");
            File.Copy(Props.compiledMapPath, Props.mapFolderPath + Props.mapName + ".bsp", true);
        }
    }
}
