﻿using System;
using System.Diagnostics;

using q2gen.generation;
using q2gen.compilation;
using q2gen.IO;

namespace q2gen
{
    class Program
    {
        static void Main(string[] args)
        {
            if (Props.compileOnly)
            {
                Compiler.CompileMap2();
                if (Props.compilerType != CompilerType.none)
                {
                    RunGame();
                }

                ConsoleExt.HeaderMSG("Done! Press any key to exit...");
                Console.ReadKey();
                return;
            }

            Console.WindowHeight = (int)(Console.LargestWindowHeight * 0.8f);

            int seed = new Random().Next();
            Props.random = new Random(seed);
            Props.seed = seed;

            ConsoleExt.HighMSG("Quake II map generator");
            ConsoleExt.Empty();

            ConsoleExt.HeaderMSG("Using following settings:");
            ConsoleExt.TableEntry("seed", seed.ToString());
            ConsoleExt.TableEntry("block size", Props.blockSize.ToString());
            ConsoleExt.TableEntry("room size", Props.roomSize.ToString());
            ConsoleExt.TableEntry("wall thickness", Props.wallThickness.ToString());
            ConsoleExt.TableEntry("segments", Props.tunnelSegments.ToString());
            ConsoleExt.TableEntry("room density", Props.roomSpawnChance.ToString());
            ConsoleExt.TableEntry("tunnel stiffness", Props.followDirectionChance.ToString());
            ConsoleExt.Separator();
            ConsoleExt.TableEntry("compiler", Props.compilerType.ToString());
            ConsoleExt.Separator();
            ConsoleExt.Empty();

            string fileString = MapGenerator.MakeMap();
            ConsoleExt.Empty();

            ConsoleExt.HeaderMSG("Writing map file...");
            Writer.WriteMap(fileString);
            ConsoleExt.Empty();

            ConsoleExt.HeaderMSG("Writing filelist...");
            Writer.WriteFilelist();
            ConsoleExt.Empty();

            Compiler.CompileMap2();
            if(Props.compilerType != CompilerType.none)
            {
                RunGame();
            }

            ConsoleExt.HeaderMSG("Done! Press any key to exit...");
            Console.ReadKey();
        }

        private static void RunGame()
        {
            Process q2 = new Process();
            q2.StartInfo.FileName = Props.gamePath;
            q2.StartInfo.Arguments = "+set game jumptest + map " + Props.mapName;

            ConsoleExt.Empty();
            ConsoleExt.HeaderMSG("Press any key to start the game...");
            Console.ReadKey();
            ConsoleExt.Empty();

            q2.Start();
            q2.WaitForExit();
        }
    }
}
