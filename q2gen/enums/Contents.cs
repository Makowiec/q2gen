﻿using System;

namespace q2gen
{
    [Flags] enum Contents
    {
        contents_window = 2,
        contents_lava = 8,
        contents_slime = 16,
        contents_water = 32,
        contents_mist = 64,
        contents_playerclip = 65536,
        contents_detail = 134217728,
        contents_ladder = 536870912
    }
}