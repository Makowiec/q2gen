﻿namespace q2gen
{
    enum TextureFit
    {
        stretch, //stretch to fit the plane
        tile,    //default texture values
        closeFit //fits as many tiles as possible and stretches them to fit the brush
    }
}