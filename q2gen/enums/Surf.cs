﻿using System;

namespace q2gen
{
    [Flags] enum Surf
    {
        surf_light = 1,
        surf_slick = 2,
        surf_sky = 4,
        surf_warp = 8,
        surf_trans33 = 16,
        surf_trans66 = 32,
        surf_flowing = 64,
        surf_nodraw = 128
    }
}