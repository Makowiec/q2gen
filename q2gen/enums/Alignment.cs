﻿namespace q2gen
{
    enum Alignment
    {
        front, back, left, right, center, frontLeft, frontRight, backLeft, backRight
    }
}