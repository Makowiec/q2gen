﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace q2gen.IO
{
    static class QWalReader
    {
        public static int[] GetWalDimensions(string path)
        {
            int[] dimensions = new int[2];

            if (!File.Exists(path))
            {
                Console.WriteLine("WARNING: texture file not found: " + path);
                dimensions[0] = 64;
                dimensions[1] = 64;
                return dimensions;
            }

            using (BinaryReader b = new BinaryReader(File.Open(path, FileMode.Open)))
            {
                int length = (int)b.BaseStream.Length;
                int position = sizeof(byte) * 32;
                int required = sizeof(uint) * 2;

                b.BaseStream.Seek(position, SeekOrigin.Begin);

                var bytes = b.ReadBytes(required);
                uint x = BitConverter.ToUInt32(bytes, 0);
                uint y = BitConverter.ToUInt32(bytes, 4);

                dimensions[0] = (int)x;
                dimensions[1] = (int)y;
            }
            return dimensions;
        }
    }
}
