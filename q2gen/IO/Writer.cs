﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace q2gen.IO
{
    static class Writer
    {
        public static void WriteMap(string mapString)
        {
            ConsoleExt.MSG("path: " + Props.mapPath);

            StreamWriter writer = new StreamWriter(Props.mapPath);
            writer.Write(mapString);
            writer.Close();

            ConsoleExt.MSG("Done!");
        }

        public static void WriteFilelist()
        {
            string s = Style.GetFilelist();
            string path = Props.mapFolder + Props.mapName + ".filelist";

            ConsoleExt.MSG("path: " + path);

            StreamWriter writer = new StreamWriter(path);
            writer.Write(s);
            writer.Close();

            ConsoleExt.MSG("Done!");
        }
    }
}
