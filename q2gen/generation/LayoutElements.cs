﻿using q2gen.objects;

namespace q2gen.generation
{
    class LayoutTunnel
    {
        public V3I layoutStart;
        public V3I layoutEnd;
        public Bounds bounds;
        public Axis axis { get; private set; }
        public Direction direction;

        public LayoutTunnel(V3I layoutStart, V3I layoutEnd, Bounds bounds, Axis axis, Direction direction)
        {
            this.layoutStart = layoutStart;
            this.layoutEnd = layoutEnd;
            this.bounds = bounds;
            this.axis = axis;
            this.direction = direction;
        }
    }

    class VerticalTunnel
    {
        public V3I layoutStart;
        public V3I layoutEnd;
        public Bounds bounds;
        public Bounds jumpBounds;
        public Direction lowDir;
        public Direction highDir;
        public int runUpDistance;
        public int jumpDistance;

        public VerticalTunnel(V3I layoutStart, V3I layoutEnd, Bounds bounds, Direction lowDir, Direction highDir, int jumpDistance, Bounds jumpBounds)
        {
            this.layoutStart = layoutStart;
            this.layoutEnd = layoutEnd;
            this.bounds = bounds;
            this.lowDir = lowDir;
            this.highDir = highDir;
            this.jumpDistance = jumpDistance;
            this.jumpBounds = jumpBounds;
        }
    }

    class LayoutTurn
    {
        public V3I layoutPosition;
        public Bounds bounds;
        public Direction direction1;
        public Direction direction2;

        public LayoutTurn(V3I layoutPosition, Bounds bounds, Direction direction1, Direction direction2)
        {
            this.layoutPosition = layoutPosition;
            this.bounds = bounds;
            this.direction1 = direction1;
            this.direction2 = direction2;
        }
    }

    class LayoutRoom
    {
        public V3I layoutMins;
        public V3I layoutMaxs;
        public Bounds bounds;
        public LayoutGate entrance;
        public LayoutGate exit;
        public Direction direction;

        public LayoutRoom(V3I layoutMins, V3I layoutMaxs, Bounds bounds, LayoutGate entrance, LayoutGate exit, Direction direction)
        {
            this.layoutMins = layoutMins;
            this.layoutMaxs = layoutMaxs;
            this.bounds = bounds;
            this.entrance = entrance;
            this.exit = exit;
            this.direction = direction;
        }
    }

    class LayoutGate
    {
        public V3I layoutPosition;
        public Bounds bounds;
        public Axis axis { get; private set; }

        public LayoutGate(V3I layoutPosition, Bounds bounds, Axis axis)
        {
            this.layoutPosition = layoutPosition;
            this.bounds = bounds;
            this.axis = axis;
        }
    }

    class LayoutStart
    {
        public V3I layoutPosition;
        public Bounds bounds;
        public Direction direction;

        public LayoutStart(V3I layoutPosition, Bounds bounds, Direction direction)
        {
            this.layoutPosition = layoutPosition;
            this.bounds = bounds;
            this.direction = direction;
        }
    }

    class LayoutEnd
    {
        public V3I layoutPosition;
        public Bounds bounds;
        public Direction direction;

        public LayoutEnd(Bounds bounds, Direction direction, V3I layoutPosition)
        {
            this.bounds = bounds;
            this.direction = direction;
            this.layoutPosition = layoutPosition;
        }
    }
}