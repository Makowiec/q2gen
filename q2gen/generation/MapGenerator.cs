﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

using q2gen.objects;
using q2gen.utils;

namespace q2gen.generation
{
    static partial class MapGenerator
    {
        public static Dictionary<Direction, V3I> directionVectors = new Dictionary<Direction, V3I>
        {
            { Direction.back, V3I.back },
            { Direction.forward, V3I.forward },
            { Direction.down, V3I.down },
            { Direction.up, V3I.up },
            { Direction.left, V3I.left },
            { Direction.right, V3I.right }
        };

        //byte values for layout array
        private static readonly byte empty = 0;
        private static readonly byte tunnel = 1;
        private static readonly byte turn = 2;       
        private static readonly byte room = 3;
        private static readonly byte entrancegate = 4;
        private static readonly byte exitgate = 5;
        private static readonly byte start = 253;
        private static readonly byte end = 254;
        private static readonly byte dirty = 255;

        //basic properties
        private static readonly int blockSize = Props.blockSize;                            //multiplications of 64 are recommended
        private static readonly int worldSize = Props.worldSize;                            //this is defined by Q2 engine and cannot be altered
        private static readonly int wallThickness = Props.wallThickness;                    //best to leave this at 64
        private static readonly int roomSize = Props.roomSize;                              //real room size = roomSize * blockSize
        private static readonly int maximumAttempts = Props.maximumAttempts;                //trying forever will most likely end up with an infinite loop
        private static readonly int tunnelSegments = Props.tunnelSegments;                  //this includes start, end, tunnels, turns and gates only
        
        private static readonly float roomSpawnChance = Props.roomSpawnChance;              //chance that we try to spawn a room when we get two blocks going in the same direction
        private static readonly float followDirectionChance = Props.followDirectionChance;  //typical chance to follow last block direction

        private static int layoutSize
        {
            get
            {
                return worldSize / blockSize;
            }
        }
        private static Vector3 offset  //layout to Q2 offset
        {
            get
            {
                return new Vector3(layoutSize / 2 * blockSize);
            }
        }                         

        //layout objects
        private static LayoutStart layoutStart;
        private static LayoutEnd layoutEnd;
        private static List<LayoutTunnel> layoutTunnels = new List<LayoutTunnel>();
        private static List<LayoutRoom> layoutRooms = new List<LayoutRoom>();
        private static Queue<LayoutRoom> roomsQueue = new Queue<LayoutRoom>();
        private static List<LayoutTurn> layoutTurns = new List<LayoutTurn>();
        private static List<VerticalTunnel> verticalTunnels = new List<VerticalTunnel>();

        private static byte[,,] layout;

        //generated objects to be parsed into file string
        private static List<Entity> entities = new List<Entity>();
        private static List<Brush> brushes = new List<Brush>();


        //variables describing generator state
        private static Stack<V3I> points = new Stack<V3I>();
        private static V3I lastPoint;
        private static Direction lastDirection;
        private static float followLastDirectionChance = 1f;
        private static int currentBlockCount = 0;

        public static string MakeMap()
        {
            string map = "{\n\"classname\" \"worldspawn\"\n";
            MapMessage.AutoGenerateMessage();
            map += MapMessage.ToString();

            ConsoleExt.HeaderMSG("Generating layout...");
            ConsoleExt.Empty();

            GenerateLayout();

            ConsoleExt.HeaderMSG("Layout");
            ConsoleExt.TableEntry("tunnels", layoutTunnels.Count.ToString());
            ConsoleExt.TableEntry("rooms", layoutRooms.Count.ToString());
            ConsoleExt.TableEntry("turns", layoutTurns.Count.ToString());
            ConsoleExt.Separator();
            ConsoleExt.Empty();
            ConsoleExt.HeaderMSG("Processing layout...");

            ProcessLayout();

            ConsoleExt.HeaderMSG("Creating map string...");

            foreach (Brush box in brushes)
            {
                map += box.ToString();
            }


            map += "}\n";

            foreach (Entity ent in entities)
            {
                map += ent.ToString();
            }
            //map += "{\n\"classname\" \"light\"\n\"origin\" \"0 0 0\"\n\"delay\" \"2\"\n\"light\" \"500\"\n\"_color\" \"1.000000 1.000000 1.000000\"\n\"_surface\" \"drom_li12_yel\"\n}\n";


            ConsoleExt.TableEntry("brush count", brushes.Count.ToString());
            ConsoleExt.TableEntry("map string length", map.Length.ToString());
            ConsoleExt.Separator();

            return map;
        }

        #region layout generation

        public static void GenerateLayout()
        {
            layout = new byte[layoutSize, layoutSize, layoutSize];

            //initial position in the middle of the map
            V3I startPos = new V3I(layoutSize / 2);
            
            //apply to layout
            layout[startPos.x, startPos.y, startPos.z] = start;
            currentBlockCount++;

            //make sure first step goes forward
            lastDirection = Direction.forward;
            followLastDirectionChance = 1f;

            //create layout start for later layout processing
            layoutStart = new LayoutStart(startPos, new Bounds(PointToCoordinate(startPos), new Vector3(blockSize / 2)), lastDirection);

            //add point to the stack and start the loop
            points.Push(startPos);

            int currentAttempts = 0;
            while(currentBlockCount < tunnelSegments && points.Count > 0)
            {
                //get the point from stack top
                V3I point = points.Peek();
                
                //retrieve point's neighbors
                Dictionary<Direction, V3I> neighbors = GetValidNeighbors(point);

                //can't go further that way (or the forced way) -> mark point dirty and go back one step
                if (neighbors.Count == 0 || (followLastDirectionChance == 1f && !neighbors.ContainsKey(lastDirection)))
                {
                    points.Pop();
                    layout[point.x, point.y, point.z] = dirty;
                    currentBlockCount--;
                    continue;
                }

                //pick random neighbor
                KeyValuePair<Direction, V3I> n;

                if (Props.RandomFloat() < followLastDirectionChance && neighbors.ContainsKey(lastDirection))
                {
                    n = new KeyValuePair<Direction, V3I>(lastDirection, neighbors[lastDirection]);
                }
                else
                {
                    n = neighbors.ElementAt(Props.random.Next(0, neighbors.Count));
                }

                switch (n.Key)
                {
                    case Direction.up:
                        followLastDirectionChance = 0.8f;
                        break;
                    case Direction.down:
                        followLastDirectionChance = 0.5f;
                        break;
                    default:
                        if (n.Key == lastDirection)
                        {
                            followLastDirectionChance = followDirectionChance;
                        }
                        else
                        {
                            followLastDirectionChance = 1f;
                        }
                        break;
                }

                //apply neighbor
                V3I neighbor = n.Value;
                if(n.Key != lastDirection)
                {
                    layout[point.x, point.y, point.z] = turn;
                }
                layout[neighbor.x, neighbor.y, neighbor.z] = tunnel;

                currentBlockCount++;
                
                //try to carve a room
                if(lastDirection == n.Key && Props.RandomFloat() < roomSpawnChance && lastDirection != Direction.up && lastDirection != Direction.down)
                {
                    //calculate room mins, maxs and exit
                    SetRoomMinsAndMaxs(neighbor, lastDirection, out V3I mins, out V3I maxs, out V3I start, out V3I exit);

                    //see if it can be made
                    if (TryMakeRoom(mins, maxs, start))
                    {
                        if (IsPointValid(exit, Tools.NegativeDirection(lastDirection)))
                        {
                            //add entrance and exit
                            layout[neighbor.x, neighbor.y, neighbor.z] = entrancegate;
                            layout[exit.x, exit.y, exit.z] = exitgate;
                            //create layout room for later processing
                            LayoutRoom room = new LayoutRoom(mins, maxs, new Bounds(((PointToCoordinate(mins) + PointToCoordinate(maxs)) / 2), (((maxs + 1).ToVector3() - mins.ToVector3()) * blockSize / 2)),
                                new LayoutGate(neighbor, new Bounds(PointToCoordinate(neighbor), new Vector3(blockSize / 2)), Tools.AxisFromDirection(lastDirection)),
                                new LayoutGate(exit, new Bounds(PointToCoordinate(exit), new Vector3(blockSize / 2)), Tools.AxisFromDirection(lastDirection)), lastDirection);
                            layoutRooms.Add(room);
                            roomsQueue.Enqueue(room);
                            //next point is now the room exit
                            neighbor = exit;
                            //make sure next goes straight
                            followLastDirectionChance = 1f;
                            //making a room means no going back
                            points.Clear();
                        }
                        else
                        {
                            //failed to make room's exit. Revert changes made by the carving method
                            CarveArea(mins, maxs, empty);
                        }
                    }
                }

                //update stuff
                points.Push(neighbor);
                lastPoint = neighbor;
                lastDirection = n.Key;

                //make sure we don't end up in an infinite loop
                currentAttempts++;
                if(currentAttempts == maximumAttempts)
                {
                    break;
                }
            }
            //mark last point as end
            layout[lastPoint.x, lastPoint.y, lastPoint.z] = end;
            layoutEnd = new LayoutEnd(new Bounds(PointToCoordinate(lastPoint), new Vector3(blockSize / 2)), Tools.NegativeDirection(lastDirection), lastPoint);

            //tunnels and turns are generated separately because of backtracking
            CreateLayoutTunnelsAndTurns();
        }

        private static void SetRoomMinsAndMaxs(V3I point, Direction direction, out V3I mins, out V3I maxs, out V3I start, out V3I exit)
        {
            switch (direction)
            {
                case Direction.forward:
                    mins = point + V3I.forward;
                    start = mins;
                    maxs = mins + new V3I(roomSize);
                    exit = maxs + V3I.forward;
                    return;
                case Direction.back:
                    maxs = point + V3I.back + V3I.up * roomSize;
                    start = point + V3I.back;
                    mins = maxs - new V3I(roomSize);
                    exit = mins + V3I.back + V3I.up * roomSize;
                    return;
                case Direction.right:
                    mins = point + V3I.right + V3I.back * roomSize;
                    start = point + V3I.right;
                    maxs = mins + new V3I(roomSize);
                    exit = maxs + V3I.right + V3I.back * roomSize;
                    return;
                default:
                    mins = point + V3I.left * (roomSize + 1);
                    start = point + V3I.left;
                    maxs = mins + new V3I(roomSize);
                    exit = maxs + V3I.left * (roomSize + 1);
                    return;
            }
        }

        private static void CreateLayoutTunnelsAndTurns()
        {
            Direction nextDirection;
            LayoutRoom room;
            List<V3I> tunnelPoints = new List<V3I>();
            Dictionary<Direction, V3I> neighbors;
            V3I current = layoutStart.layoutPosition;
            lastDirection = layoutStart.direction;
            byte lastValue = start;
            byte currentValue;

            bool chainEnded = false;

            while (!chainEnded)
            {
                V3I next = current + directionVectors[lastDirection];
                if(!FitsInLayout(next))
                {
                    chainEnded = true;
                    ConsoleExt.WarningMSG("The map might be incomplete. Please validate it. (reason: next point outside of bounds)");
                    continue;
                }
                currentValue = layout[next.x, next.y, next.z];

                if(currentValue == tunnel)
                {
                    tunnelPoints.Add(next);
                    current = next;
                    continue;
                }
                else if(currentValue == turn)
                {
                    CreateLayoutTunnel(tunnelPoints, lastDirection);
                    tunnelPoints.Clear();
                    neighbors = GetNeighbors(next, Tools.NegativeDirection(lastDirection));
                    try
                    {
                        nextDirection = neighbors.First(i => layout[i.Value.x, i.Value.y, i.Value.z] == tunnel || layout[i.Value.x, i.Value.y, i.Value.z] == turn ||
                        layout[i.Value.x, i.Value.y, i.Value.z] == entrancegate || layout[i.Value.x, i.Value.y, i.Value.z] == end).Key;
                    }
                    catch
                    {
                        chainEnded = true;
                        ConsoleExt.ErrorMSG("The map will be incomplete. (reason: neighbor search failed)");
                        continue;
                    }
                    
                    layoutTurns.Add(new LayoutTurn(next, new Bounds(PointToCoordinate(next), new Vector3(blockSize / 2)), lastDirection, nextDirection));
                    lastDirection = nextDirection;
                }
                else if(currentValue == entrancegate)
                {
                    CreateLayoutTunnel(tunnelPoints, lastDirection);
                    tunnelPoints.Clear();
                    room = roomsQueue.Dequeue();
                    next = room.exit.layoutPosition;
                }
                else if(currentValue == end)
                {
                    CreateLayoutTunnel(tunnelPoints, lastDirection);
                    chainEnded = true;
                }
                current = next;
            }
        }

        private static void CreateLayoutTunnel(List<V3I> points, Direction direction)
        {
            if(points.Count == 0) { return; }
            Vector3 center = new Vector3();
            foreach(V3I p in points)
            {
                center += PointToCoordinate(p);
            }
            center /= points.Count;
            Axis axis = Tools.AxisFromDirection(direction);
            Vector3 extents;
            switch (axis)
            {
                case Axis.x:
                    extents = new Vector3(points.Count * blockSize / 2, blockSize / 2, blockSize / 2);
                    break;
                case Axis.y:
                    extents = new Vector3(blockSize / 2, points.Count * blockSize / 2, blockSize / 2);
                    if(lastDirection == Direction.up)
                    {
                        CreateVerticalTunnel(points);
                    }
                    break;
                default:
                    extents = new Vector3(blockSize / 2, blockSize / 2, points.Count * blockSize / 2);
                    break;
            }
            LayoutTunnel tunnel = new LayoutTunnel(points[0], points[points.Count - 1], new Bounds(center, extents), axis, direction);
            layoutTunnels.Add(tunnel);
        }

        private static void CreateVerticalTunnel(List<V3I> points)
        {
            Vector3 center = new Vector3();
            foreach (V3I p in points)
            {
                center += PointToCoordinate(p);
            }
            center /= points.Count;
            Vector3 extents = new Vector3(blockSize / 2, (points.Count + 2) * blockSize / 2, blockSize / 2);
            int jumpDistance = (points.Count + 1) * blockSize;

            Bounds jumpBounds = new Bounds(center - new Vector3(0, blockSize / 2, 0), extents - new Vector3(0, blockSize / 2, 0));

            V3I bottom = points.OrderBy(i => i.y).First() + V3I.down;
            V3I top = points.OrderByDescending(i => i.y).First() + V3I.up;

            var n = GetNeighbors(bottom, Direction.up);

            Direction bottomDirection = n.First(i => layout[i.Value.x, i.Value.y, i.Value.z] != empty && layout[i.Value.x, i.Value.y, i.Value.z] != dirty).Key;

            n = GetNeighbors(top, Direction.down);

            Direction topDirection = n.First(i => layout[i.Value.x, i.Value.y, i.Value.z] != empty && layout[i.Value.x, i.Value.y, i.Value.z] != dirty).Key;

            VerticalTunnel tunnel = new VerticalTunnel(bottom, top, new Bounds(center, extents), bottomDirection, topDirection, jumpDistance, jumpBounds);
            verticalTunnels.Add(tunnel);
        }

        private static Dictionary<Direction, V3I> GetValidNeighbors(V3I point)
        {
            Dictionary<Direction, V3I> neighbors = new Dictionary<Direction, V3I>();

            if(IsPointValid(point + V3I.right, Direction.left))
            {
                neighbors.Add(Direction.right, point + V3I.right);
            }
            if (IsPointValid(point + V3I.left, Direction.right))
            {
                neighbors.Add(Direction.left, point + V3I.left);
            }

            if (IsPointValid(point + V3I.up, Direction.down))
            {
                neighbors.Add(Direction.up, point + V3I.up);
            }
            if (IsPointValid(point + V3I.down, Direction.up))
            {
                neighbors.Add(Direction.down, point + V3I.down);
            }

            if (IsPointValid(point + V3I.forward, Direction.back))
            {
                neighbors.Add(Direction.forward, point + V3I.forward);
            }
            if (IsPointValid(point + V3I.back, Direction.forward))
            {
                neighbors.Add(Direction.back, point + V3I.back);
            }
            return neighbors;
        }

        private static Dictionary<Direction, V3I> GetNeighbors(V3I point, Direction ignoredDirection)
        {
            Dictionary<Direction, V3I> neighbors = new Dictionary<Direction, V3I>();

            if (FitsInLayout(point + V3I.right) && ignoredDirection != Direction.right)
            {
                neighbors.Add(Direction.right, point + V3I.right);
            }
            if (FitsInLayout(point + V3I.left) && ignoredDirection != Direction.left)
            {
                neighbors.Add(Direction.left, point + V3I.left);
            }

            if (FitsInLayout(point + V3I.up) && ignoredDirection != Direction.up)
            {
                neighbors.Add(Direction.up, point + V3I.up);
            }
            if (FitsInLayout(point + V3I.down) && ignoredDirection != Direction.down)
            {
                neighbors.Add(Direction.down, point + V3I.down);
            }

            if (FitsInLayout(point + V3I.forward) && ignoredDirection != Direction.forward)
            {
                neighbors.Add(Direction.forward, point + V3I.forward);
            }
            if (FitsInLayout(point + V3I.back) && ignoredDirection != Direction.back)
            {
                neighbors.Add(Direction.back, point + V3I.back);
            }
            return neighbors;
        }

        private static Dictionary<Direction, V3I> GetNeighborsOfType(V3I point, byte type)
        {
            Dictionary<Direction, V3I> neighbors = new Dictionary<Direction, V3I>();
            V3I newpoint = point + V3I.right;
            byte value = layout[newpoint.x, newpoint.y, newpoint.z];
            if (FitsInLayout(newpoint) && value == type)
            {
                neighbors.Add(Direction.right, newpoint);
            }
            newpoint = point + V3I.left;
            value = layout[newpoint.x, newpoint.y, newpoint.z];
            if (FitsInLayout(newpoint) && value == type)
            {
                neighbors.Add(Direction.left, newpoint);
            }
            newpoint = point + V3I.up;
            value = layout[newpoint.x, newpoint.y, newpoint.z];
            if (FitsInLayout(newpoint) && value == type)
            {
                neighbors.Add(Direction.up, newpoint);
            }
            newpoint = point + V3I.down;
            value = layout[newpoint.x, newpoint.y, newpoint.z];
            if (FitsInLayout(newpoint) && value == type)
            {
                neighbors.Add(Direction.down, newpoint);
            }
            newpoint = point + V3I.forward;
            value = layout[newpoint.x, newpoint.y, newpoint.z];
            if (FitsInLayout(newpoint) && value == type)
            {
                neighbors.Add(Direction.forward, newpoint);
            }
            newpoint = point + V3I.back;
            value = layout[newpoint.x, newpoint.y, newpoint.z];
            if (FitsInLayout(newpoint) && value == type)
            {
                neighbors.Add(Direction.back, newpoint);
            }
            value = layout[newpoint.x, newpoint.y, newpoint.z];
            return neighbors;
        }

        private static Dictionary<Direction, V3I> GetNeighborsOfType(V3I point, byte type, Direction ignoredDirection)
        {
            Dictionary<Direction, V3I> neighbors = new Dictionary<Direction, V3I>();
            V3I newpoint = point + V3I.right;
            byte value = layout[newpoint.x, newpoint.y, newpoint.z];
            if (FitsInLayout(newpoint) && value == type && ignoredDirection != Direction.right)
            {
                neighbors.Add(Direction.right, newpoint);
            }
            newpoint = point + V3I.left;
            value = layout[newpoint.x, newpoint.y, newpoint.z];
            if (FitsInLayout(newpoint) && value == type && ignoredDirection != Direction.left)
            {
                neighbors.Add(Direction.left, newpoint);
            }
            newpoint = point + V3I.up;
            value = layout[newpoint.x, newpoint.y, newpoint.z];
            if (FitsInLayout(newpoint) && value == type && ignoredDirection != Direction.up)
            {
                neighbors.Add(Direction.up, newpoint);
            }
            newpoint = point + V3I.down;
            value = layout[newpoint.x, newpoint.y, newpoint.z];
            if (FitsInLayout(newpoint) && value == type && ignoredDirection != Direction.down)
            {
                neighbors.Add(Direction.down, newpoint);
            }
            newpoint = point + V3I.forward;
            value = layout[newpoint.x, newpoint.y, newpoint.z];
            if (FitsInLayout(newpoint) && value == type && ignoredDirection != Direction.forward)
            {
                neighbors.Add(Direction.forward, newpoint);
            }
            newpoint = point + V3I.back;
            value = layout[newpoint.x, newpoint.y, newpoint.z];
            if (FitsInLayout(newpoint) && value == type && ignoredDirection != Direction.back)
            {
                neighbors.Add(Direction.back, newpoint);
            }
            value = layout[newpoint.x, newpoint.y, newpoint.z];
            return neighbors;
        }

        private static bool IsPointValid(V3I point)
        {
            if (!FitsInLayout(point) || layout[point.x, point.y + 1, point.z] != empty || layout[point.x, point.y - 1, point.z] != empty || 
                layout[point.x - 1, point.y, point.z] != empty || layout[point.x + 1, point.y, point.z] != empty
                ||  layout[point.x, point.y, point.z + 1] != empty)
            {
                return false;
            }
            return true;
        }

        private static bool IsPointValid(V3I point, Direction ignoredDirection)
        {
            if (FitsInLayout(point))
            {
                if (ignoredDirection != Direction.up && layout[point.x, point.y + 1, point.z] != empty)
                {
                    return false;
                }
                if (ignoredDirection != Direction.down && layout[point.x, point.y - 1, point.z] != empty)
                {
                    return false;
                }
                if (ignoredDirection != Direction.left && layout[point.x - 1, point.y, point.z] != empty)
                {
                    return false;
                }
                if (ignoredDirection != Direction.right && layout[point.x + 1, point.y, point.z] != empty)
                {
                    return false;
                }
                if (ignoredDirection != Direction.back && layout[point.x, point.y, point.z - 1] != empty)
                {
                    return false;
                }
                if (ignoredDirection != Direction.forward && layout[point.x, point.y, point.z + 1] != empty)
                {
                    return false;
                }
                return true;
            }
            return false;
        }

        private static bool TryMakeRoom(V3I mins, V3I maxs, V3I startPoint)
        {
            for (int x = mins.x; x <= maxs.x; x++)
            {
                for (int y = mins.y; y <= maxs.y; y++)
                {
                    for (int z = mins.z; z <= maxs.z; z++)
                    {
                        V3I point = new V3I(x, y, z);
                        if(point == startPoint)
                        {
                            continue;
                        }
                        if(!IsPointValid(point))
                        {
                            return false;
                        }
                    }
                }
            }
            CarveArea(mins, maxs, room);
            return true;
        }

        private static void CarveArea(V3I mins, V3I maxs, byte type)
        {
            for (int x = mins.x; x <= maxs.x; x++)
            {
                for(int y = mins.y; y <= maxs.y; y++)
                {
                    for(int z = mins.z; z <=maxs.z; z++)
                    {
                        if(FitsInLayout(x, y, z))
                        {
                            layout[x, y, z] = type;
                        }
                    }
                }
            }
        }

        private static bool FitsInLayout(int x, int y, int z)
        {
            return x >= 1 && x < layout.GetLength(0) - 1 && y >= 1 && y < layout.GetLength(1) - 2 && z >= 1 && z < layout.GetLength(2) - 1;
        }

        private static bool FitsInLayout(V3I vector)
        {
            return FitsInLayout(vector.x, vector.y, vector.z);
        }

        #endregion

        #region layout processing

        private static void ProcessLayout()
        {
            ConsoleExt.MSG(" Creating walls...");
            CreateOuterWalls();

            ConsoleExt.MSG(" Vertical tunnels...");
            CreateVerticalTunnelFeatures();

            ConsoleExt.MSG(" Creating details...");
            CreateDetails();

            ConsoleExt.MSG(" Creating lights...");
            CreateCeilingLights();

            ConsoleExt.MSG(" Creating entities...");
            CreateEntities();

            ConsoleExt.Empty();
        }

        private static void CreateCeilingLights()
        {
            List<Box> boxes = new List<Box>();
            Vector3 heightOffset = new Vector3(0, blockSize / 2, 0);

            //tunnels
            foreach(LayoutTunnel t in layoutTunnels)
            {
                if(t.axis == Axis.y) { continue; }

                int distance = 0;
                switch (t.axis)
                {
                    case Axis.x:
                        distance = Math.Abs(t.layoutEnd.x - t.layoutStart.x);
                        break;
                    case Axis.z:
                        distance = Math.Abs(t.layoutEnd.z - t.layoutStart.z);
                        break;
                }

                for(int i = 0; i <= distance; i++)
                {
                    boxes.AddRange(Structure.MakeCeilingLight(PointToCoordinate(t.layoutStart) +
                        i * blockSize * directionVectors[t.direction].ToVector3() + heightOffset, 1000, false));
                }
            }

            //rooms
            foreach(LayoutRoom r in layoutRooms)
            {
                //room ceiling
                for(int x = (int)r.bounds.mins.X + blockSize / 2; x <= r.bounds.maxs.X - blockSize / 2; x += blockSize)
                {
                    for (int z = (int)r.bounds.mins.Z + blockSize / 2; z <= r.bounds.maxs.Z - +blockSize / 2; z += blockSize)
                    {
                        boxes.AddRange(Structure.MakeCeilingLight(new Vector3(x, r.bounds.maxs.Y, z), 2000, false));
                    }
                }
                //entrances
                boxes.AddRange(Structure.MakeCeilingLight(r.entrance.bounds.center + heightOffset, 800, false));
                boxes.AddRange(Structure.MakeCeilingLight(r.exit.bounds.center + heightOffset, 800, false));
            }

            //corners (doesn't work properly yet)
            //foreach(LayoutTurn t in layoutTurns)
            //{
            //    if(t.direction2 == Direction.up) { continue; }

            //    boxes.AddRange(Structure.MakeCeilingLight(t.bounds.center + heightOffset, 1200));
            //}

            brushes.AddRange(boxes);
        }

        private static void CreateDetails()
        {
            brushes.AddRange(Structure.MakeGate(layoutStart.bounds, layoutStart.direction, blockSize, false));

            foreach(LayoutRoom r in layoutRooms)
            {
                brushes.AddRange(Structure.MakeGate(r.entrance.bounds, r.direction, blockSize, false));
                brushes.AddRange(Structure.MakeGate(r.exit.bounds, Tools.NegativeDirection(r.direction), blockSize, false));
            }
        }

        private static void CreateVerticalTunnelFeatures()
        {
            foreach(VerticalTunnel t in verticalTunnels)
            {
                //brushes.AddRange(Structure.MakeLadder(t.bounds, t.highDir, 80, t.lowDir == t.highDir ? 80 : 8, 80 + blockSize));
                //brushes.AddRange(Structure.MakeVerticalMulti(t.bounds, t.lowDir, t.highDir));
                brushes.AddRange(Structure.MakeVerticalStructure(t.bounds, t.lowDir, t.highDir));
            }

            List<LayoutTurn> turns = layoutTurns.Where(i => i.direction2 == Direction.up && layout[i.layoutPosition.x, i.layoutPosition.y + 1, i.layoutPosition.z] == turn).ToList();
            
            foreach(LayoutTurn t in turns)
            {
                Direction targetDir = layoutTurns.First(i => i.layoutPosition == t.layoutPosition + V3I.up).direction2;
                //brushes.AddRange(Structure.MakeStandingDoubleJump(t.bounds, Tools.EulerAngleForwardToDirection(targetDir), Tools.RandomAdjacentAlignment(targetDir)));
                //brushes.AddRange(Structure.MakeVerticalMulti(t.bounds, t.direction1, targetDir, blockSize));
                brushes.AddRange(Structure.MakeDJandLadder(t.bounds, targetDir));
            }

            foreach(LayoutRoom r in layoutRooms)
            {
                brushes.AddRange(Structure.MakeRoomIceRamp(r.bounds, r.direction));
                //brushes.AddRange(Structure.MakeRoomPlatforms(r.bounds, r.direction, roomSize));

                //Vector3 center = new Vector3(r.exit.bounds.center.X, r.bounds.center.Y - (blockSize / 2), r.exit.bounds.center.Z) - directionVectors[r.direction].ToVector3() * blockSize;
                //Vector3 extents = new Vector3(32, r.bounds.extents.Y - (blockSize / 2), 32);

                //Box newbox = new Box(center, extents, Style.detailTexture);
                //newbox.SetContents((int)Contents.contents_ladder);
                //newbox.SetSurfaces((int)Surf.surf_light);
                //newbox.SetValues(60);

                //brushes.Add(newbox);
            }
        }

        private static void CreateEntities()
        {
            int platformHeightExtent = 4;
            //Texture platformTop = style.floorTexture;
            Vector3 platformCenter;
            Vector3 platformExtents;

            //spawn
            entities.Add(MakeInfo_player_start(layoutStart, platformHeightExtent * 2));
            platformCenter = new Vector3(layoutStart.bounds.center.X, layoutStart.bounds.mins.Y + platformHeightExtent, layoutStart.bounds.center.Z);
            platformExtents = new Vector3(32, platformHeightExtent, 32);
            Box platform = new Box(platformCenter, platformExtents, Style.detailTexture);
            platform.SetSurfaces(Surf.surf_light);
            platform.SetValues(160);
            //platform.up.texture = platformTop;
            brushes.Add(platform);

            //finish
            entities.Add(MakeRailgun(layoutEnd, platformHeightExtent * 2));
            platformCenter = new Vector3(layoutEnd.bounds.center.X, layoutEnd.bounds.mins.Y + platformHeightExtent, layoutEnd.bounds.center.Z);
            platform = new Box(platformCenter, platformExtents, Style.detailTexture);
            platform.SetSurfaces(Surf.surf_light);
            platform.SetValues(160);
            //platform.up.texture = platformTop;
            brushes.Add(platform);

            CreateSimpleLightEnts();
        }

        private static void CreateOuterWalls()
        {
            List<Brush> b = new List<Brush>();

            foreach (var t in layoutTunnels)
            {
                b.AddRange(CreateTunnelWalls(t));
            }
            foreach (var t in layoutRooms)
            {
                b.AddRange(CreateRoomWalls(t));
                b.AddRange(CreateGateWalls(t.entrance));
                b.AddRange(CreateGateWalls(t.exit));
            }
            foreach (var t in layoutTurns)
            {
                b.AddRange(CreateTurnWalls(t));
            }

            b.AddRange(CreateWallsAroundBounds(layoutStart.bounds, new List<Direction> { layoutStart.direction }));
            b.AddRange(CreateWallsAroundBounds(layoutEnd.bounds, new List<Direction> { layoutEnd.direction }));

            brushes.AddRange(b);
        }

        private static List<Brush> CreateTunnelWalls(LayoutTunnel tunnel)
        {
            List<Direction> directions = new List<Direction>();
            switch (tunnel.axis)
            {
                case Axis.x:
                    directions.Add(Direction.right);
                    directions.Add(Direction.left);
                    break;
                case Axis.y:
                    directions.Add(Direction.up);
                    directions.Add(Direction.down);
                    break;
                case Axis.z:
                    directions.Add(Direction.back);
                    directions.Add(Direction.forward);
                    break;
            }
            return CreateWallsAroundBounds(tunnel.bounds, directions);
        }

        private static List<Brush> CreateRoomWalls(LayoutRoom room)
        {
            List<Brush> brushes = new List<Brush>();
            Vector3 center;
            Vector3 extents;
            Vector3 center1;
            Vector3 center2;
            Vector3 extents1;
            Vector3 extents2;
            foreach (var v in directionVectors)
            {
                switch (v.Key)
                {
                    case Direction.up:
                        center = new Vector3(room.bounds.center.X, room.bounds.maxs.Y + wallThickness / 2, room.bounds.center.Z);
                        extents = new Vector3(room.bounds.extents.X, wallThickness / 2, room.bounds.extents.Z);
                        break;
                    case Direction.down:
                        center = new Vector3(room.bounds.center.X, room.bounds.mins.Y - wallThickness / 2, room.bounds.center.Z);
                        extents = new Vector3(room.bounds.extents.X, wallThickness / 2, room.bounds.extents.Z);
                        break;
                    case Direction.left:
                        center = new Vector3(room.bounds.mins.X - wallThickness / 2, room.bounds.center.Y, room.bounds.center.Z);
                        extents = new Vector3(wallThickness / 2, room.bounds.extents.Y, room.bounds.extents.Z);
                        break;
                    case Direction.right:
                        center = new Vector3(room.bounds.maxs.X + wallThickness / 2, room.bounds.center.Y, room.bounds.center.Z);
                        extents = new Vector3(wallThickness / 2, room.bounds.extents.Y, room.bounds.extents.Z);
                        break;
                    case Direction.back:
                        center = new Vector3(room.bounds.center.X, room.bounds.center.Y, room.bounds.mins.Z - wallThickness / 2);
                        extents = new Vector3(room.bounds.extents.X, room.bounds.extents.Y, wallThickness / 2);
                        break;
                    default: //forward
                        center = new Vector3(room.bounds.center.X, room.bounds.center.Y, room.bounds.maxs.Z + wallThickness / 2);
                        extents = new Vector3(room.bounds.extents.X, room.bounds.extents.Y, wallThickness / 2);
                        break;
                }

                if (v.Key == Tools.NegativeDirection(room.direction) || v.Key == room.direction)
                {
                    float difference = blockSize / 2;
                    switch (v.Key)
                    {
                        case Direction.left:
                            center1 = center + new Vector3(0, 0, -difference);
                            center2 = center + new Vector3(0, blockSize / 2 * (v.Key == room.direction ? -1 : 1), difference * (roomSize));
                            extents1 = extents - new Vector3(0, 0, blockSize / 2);
                            extents2 = new Vector3(extents.X, extents.Y - blockSize / 2, blockSize / 2);
                            break;
                        case Direction.right:
                            center1 = center + new Vector3(0, 0, difference);
                            center2 = center + new Vector3(0, blockSize / 2 * (v.Key == room.direction ? -1 : 1), -difference * (roomSize));
                            extents1 = extents - new Vector3(0, 0, blockSize / 2);
                            extents2 = new Vector3(extents.X, extents.Y - blockSize / 2, blockSize / 2);
                            break;
                        case Direction.back:
                            center1 = center + new Vector3(difference, 0, 0);
                            center2 = center + new Vector3(-difference * (roomSize), blockSize / 2 * (v.Key == room.direction ? -1 : 1), 0);
                            extents1 = extents - new Vector3(blockSize / 2, 0, 0);
                            extents2 = new Vector3(blockSize / 2, extents.Y - blockSize / 2, extents.Z);
                            break;
                        default: //forward
                            center1 = center + new Vector3(-difference, 0, 0);
                            center2 = center + new Vector3(difference * (roomSize), blockSize / 2 * (v.Key == room.direction ? -1 : 1), 0);
                            extents1 = extents - new Vector3(blockSize / 2, 0, 0);
                            extents2 = new Vector3(blockSize / 2, extents.Y - blockSize / 2, extents.Z);
                            break;
                    }
                    brushes.Add(TrapezoidWall(center1, extents1, v.Key, wallThickness, Style.wallTexture));
                    brushes.Add(TrapezoidWall(center2, extents2, v.Key, wallThickness, Style.wallTexture));
                }
                else
                {
                    switch (v.Key)
                    {
                        case Direction.up:
                            brushes.Add(TrapezoidWall(center, extents, v.Key, wallThickness, Style.ceilingTexture));
                            break;
                        case Direction.down:
                            brushes.Add(TrapezoidWall(center, extents, v.Key, wallThickness, Style.floorTexture));
                            break;
                        default:
                            brushes.Add(TrapezoidWall(center, extents, v.Key, wallThickness, Style.wallTexture));
                            break;
                    }
                }
            }

            return brushes;
        }

        private static List<Brush> CreateTurnWalls(LayoutTurn turn)
        {
            List<Direction> directions = new List<Direction> { Tools.NegativeDirection(turn.direction1), turn.direction2 };
            return CreateWallsAroundBounds(turn.bounds, directions);
        }

        private static List<Brush> CreateGateWalls(LayoutGate gate)
        {
            List<Direction> directions = new List<Direction>();
            switch (gate.axis)
            {
                case Axis.x:
                    directions.Add(Direction.right);
                    directions.Add(Direction.left);
                    break;
                case Axis.y:
                    directions.Add(Direction.up);
                    directions.Add(Direction.down);
                    break;
                case Axis.z:
                    directions.Add(Direction.back);
                    directions.Add(Direction.forward);
                    break;
            }
            return CreateWallsAroundBounds(gate.bounds, directions);
        }

        private static List<Brush> CreateWallsAroundBounds(Bounds bounds, List<Direction> ignoredDirections)
        {
            List<Brush> brushes = new List<Brush>();

            Vector3 center;
            Vector3 extents;
            foreach (var v in directionVectors)
            {
                if (ignoredDirections.Contains(v.Key)) { continue; }

                switch (v.Key)
                {
                    case Direction.up:
                        center = new Vector3(bounds.center.X, bounds.maxs.Y + wallThickness / 2, bounds.center.Z);
                        extents = new Vector3(bounds.extents.X, wallThickness / 2, bounds.extents.Z);
                        break;
                    case Direction.down:
                        center = new Vector3(bounds.center.X, bounds.mins.Y - wallThickness / 2, bounds.center.Z);
                        extents = new Vector3(bounds.extents.X, wallThickness / 2, bounds.extents.Z);
                        break;
                    case Direction.left:
                        center = new Vector3(bounds.mins.X - wallThickness / 2, bounds.center.Y, bounds.center.Z);
                        extents = new Vector3(wallThickness / 2, bounds.extents.Y, bounds.extents.Z);
                        break;
                    case Direction.right:
                        center = new Vector3(bounds.maxs.X + wallThickness / 2, bounds.center.Y, bounds.center.Z);
                        extents = new Vector3(wallThickness / 2, bounds.extents.Y, bounds.extents.Z);
                        break;
                    case Direction.back:
                        center = new Vector3(bounds.center.X, bounds.center.Y, bounds.mins.Z - wallThickness / 2);
                        extents = new Vector3(bounds.extents.X, bounds.extents.Y, wallThickness / 2);
                        break;
                    default: //forward
                        center = new Vector3(bounds.center.X, bounds.center.Y, bounds.maxs.Z + wallThickness / 2);
                        extents = new Vector3(bounds.extents.X, bounds.extents.Y, wallThickness / 2);
                        break;
                }

                switch (v.Key)
                {
                    case Direction.up:
                        brushes.Add(TrapezoidWall(center, extents, v.Key, wallThickness, Style.ceilingTexture));
                        break;
                    case Direction.down:
                        brushes.Add(TrapezoidWall(center, extents, v.Key, wallThickness, Style.floorTexture));
                        break;
                    default:
                        brushes.Add(TrapezoidWall(center, extents, v.Key, wallThickness, Style.wallTexture));
                        break;
                }
                
            }

            return brushes;
        }

        #endregion

        #region brush generators

        private static Box TrapezoidWall(Vector3 center, Vector3 extents, Direction direction, int slantDistance, Texture texture)
        {
            Vector3 mins = center - extents;
            Vector3 maxs = center + extents;

            return TrapezoidWall(mins, maxs, slantDistance, direction, texture);
        }

        private static Box TrapezoidWall(Vector3 mins, Vector3 maxs, int slantDistance, Direction direction, Texture texture)
        {
            Vector3 bottomBackLeft = mins;
            Vector3 bottomFrontLeft = new Vector3(mins.X, mins.Y, maxs.Z);
            Vector3 bottomFrontRight = new Vector3(maxs.X, mins.Y, maxs.Z);
            Vector3 bottomBackRight = new Vector3(maxs.X, mins.Y, mins.Z);

            Vector3 upBackLeft = new Vector3(mins.X, maxs.Y, mins.Z);
            Vector3 upFrontLeft = new Vector3(mins.X, maxs.Y, maxs.Z);
            Vector3 upFrontRight = maxs;
            Vector3 upBackRight = new Vector3(maxs.X, maxs.Y, mins.Z);

            switch (direction)
            {
                case Direction.back:
                    bottomBackLeft += new Vector3(slantDistance, slantDistance, 0);
                    bottomBackRight += new Vector3(-slantDistance, slantDistance, 0);
                    upBackLeft += new Vector3(slantDistance, -slantDistance, 0);
                    upBackRight += new Vector3(-slantDistance, -slantDistance, 0);
                    break;
                case Direction.forward:
                    bottomFrontLeft += new Vector3(slantDistance, slantDistance, 0);
                    bottomFrontRight += new Vector3(-slantDistance, slantDistance, 0);
                    upFrontLeft += new Vector3(slantDistance, -slantDistance, 0);
                    upFrontRight += new Vector3(-slantDistance, -slantDistance, 0);
                    break;
                case Direction.left:
                    bottomFrontLeft += new Vector3(0, slantDistance, -slantDistance);
                    bottomBackLeft += new Vector3(0, slantDistance, slantDistance);
                    upFrontLeft += new Vector3(0, -slantDistance, -slantDistance);
                    upBackLeft += new Vector3(0, -slantDistance, slantDistance);
                    break;
                case Direction.right:
                    bottomFrontRight += new Vector3(0, slantDistance, -slantDistance);
                    bottomBackRight += new Vector3(0, slantDistance, slantDistance);
                    upFrontRight += new Vector3(0, -slantDistance, -slantDistance);
                    upBackRight += new Vector3(0, -slantDistance, slantDistance);
                    break;
                case Direction.up:
                    upBackLeft += new Vector3(slantDistance, 0, slantDistance);
                    upFrontLeft += new Vector3(slantDistance, 0, -slantDistance);
                    upFrontRight += new Vector3(-slantDistance, 0, -slantDistance);
                    upBackRight += new Vector3(-slantDistance, 0, slantDistance);
                    break;
                case Direction.down:
                    bottomBackLeft += new Vector3(slantDistance, 0, slantDistance);
                    bottomFrontLeft += new Vector3(slantDistance, 0, -slantDistance);
                    bottomFrontRight += new Vector3(-slantDistance, 0, -slantDistance);
                    bottomBackRight += new Vector3(-slantDistance, 0, slantDistance);
                    break;
            }

            return new Box(bottomBackLeft, bottomFrontLeft, bottomFrontRight, bottomBackRight, upBackLeft, upFrontLeft, upFrontRight, upBackRight, texture);
        }

        #endregion

        #region entity generators

        private static Entity MakeInfo_player_start(LayoutStart layoutStart, int platfromOffset)
        {
            string classname = "info_player_start";
            string angle = Tools.DirectionToAngle(layoutStart.direction).ToString();
            Vector3 origin = layoutStart.bounds.center - new Vector3(0, layoutStart.bounds.extents.Y - 24 - platfromOffset, 0);

            Entity ent = new Entity(classname, origin);
            ent.properties.Add("angle", angle);
            return ent;
        }

        private static Entity MakeRailgun(LayoutEnd layoutEnd, int platfromOffset)
        {
            string classname = "weapon_railgun";
            Vector3 origin = layoutEnd.bounds.center - new Vector3(0, layoutEnd.bounds.extents.Y - 16 - platfromOffset, 0);

            return new Entity(classname, origin);
        }

        private static Entity MakeLightEntity(Vector3 origin, int strength)
        {
            string classname = "light";
            string light = strength.ToString();

            Entity ent = new Entity(classname, origin);
            ent.properties.Add("light", light);
            return ent;
        }

        private static void CreateSimpleLightEnts()
        {
            //foreach (var t in layoutTurns)
            //{
            //    if (t.direction2 != Direction.up)
            //    {
            //        entities.Add(MakeLightEntity(t.bounds.center, 180));
            //    }
            //}
            //foreach (var t in verticalTunnels)
            //{
            //    entities.Add(MakeLightEntity(t.bounds.center, 200));
            //}
        }

        #endregion

        #region tools

        private static Vector3 PointToCoordinate(V3I point)
        {
            return new Vector3(point.x, point.y, point.z) * blockSize - offset;// + new Vector3(32);
        }

        public static void AddGizmo(Vector3 position)
        {
            
            Box gizmo = new Box(position, new Vector3(6), Style.gizmo, false);
            gizmo.AddSurfaceContents(Surf.surf_light);
            gizmo.SetValues(100);
            gizmo.AddContents(Contents.contents_detail | Contents.contents_mist);

            brushes.Add(gizmo);
        }

        #endregion
    }
}
