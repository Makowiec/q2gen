﻿using System;
using System.Collections.Generic;
using System.Numerics;

using q2gen.objects;
using q2gen.utils;

namespace q2gen.generation
{
    static class Structure
    {
        //ladder properties
        private static readonly int ladderBarThickness = 8;
        private static readonly int ladderClipThickness = 2;

        //gate properties
        private static readonly int gateThickness = 32;
        private static readonly int gateInnerThickness = 16;
        private static readonly int gateCornerSize = 16;

        //light properties
        private static readonly int ceilingLightThickness = 4;
        private static readonly int ceilingLightSize = 48;
        private static readonly int ceilingLightBorder = 2;

        //ice room properties
        private static readonly int icePlatHeight = 8;
        private static readonly int icePlatWallOffset = 192;
        private static readonly int iceRampSize = 64;

        #region full bounds generators
        //These are meant to fill up entire bounds provided either with chains or single jumps.

        /// <summary>
        /// Creates an ice platform coupled with a ramp to jump off of.
        /// </summary>
        /// <param name="bounds">Target room bounds.</param>
        /// <param name="direction">Room's direction. The ramp will be facing that way.</param>
        /// <returns>List of boxes making the structure.</returns>
        public static List<Box> MakeRoomIceRamp(Bounds bounds, Direction direction)
        {
            List<Box> boxes = new List<Box>();

            //Make an ice platform.
            Vector3 platformExtents = new Vector3(bounds.extents.X - icePlatWallOffset, icePlatHeight / 2, bounds.extents.Z - icePlatWallOffset);
            Box platform = new Box(Vector3.Zero, platformExtents, Style.ceilingTexture);
            platform.up.surface = Surf.surf_slick | Surf.surf_light; //Give the platform slick (ice) surface property and a little bit of light.
            platform.up.value = 5;
            platform.up.Texture = Style.ice;

            boxes.Add(platform);

            //Make a ramp and place it correctly on the platform.
            Vector3 center = platformExtents - new Vector3(1, -1, 1) * iceRampSize / 2;
            Ramp ramp = new Ramp(center, new Vector3(iceRampSize / 2), Style.detailTexture);
            ramp.SetContents(Contents.contents_detail);
            ramp.back.Texture = Texture.Clone(Style.ceilingTexture);
            ramp.back.surface |= Surf.surf_slick;
            boxes.Add(ramp);

            AlignStructureInBounds(bounds, boxes, Tools.EulerAngleForwardToDirection(direction), Alignment.center);

            return boxes;
        }

        /// <summary>
        /// Generates a full vertical structure filling up entire bounds. Including multis, djs, ladders, floors and ramps.
        /// </summary>
        /// <param name="bounds">Target bounds.</param>
        /// <param name="inDirection">Direction of incoming.</param>
        /// <param name="outDirection">Direction of outgoing tunnel.</param>
        /// <returns>List of boxes making the structure.</returns>
        public static List<Box> MakeVerticalStructure(Bounds bounds, Direction inDirection, Direction outDirection)
        {
            int totalHeight;
            Bounds totalBounds;
            bool lastWasFloor = false;
            int maxHeight = bounds.height - Props.blockSize;
            List<Box> boxes = new List<Box>();

            //the first jump is always a multi
            List<Box> multi = MakeVerticalMulti(bounds, inDirection);
            boxes.AddRange(multi);

            //calculate total height of the generated structure
            totalBounds = Transform.GetStructureBounds(multi);
            totalHeight = (int)totalBounds.extents.Y * 2;

            //we're already high enough, return
            if (totalHeight >= maxHeight) { return boxes; }

            //decide what structures to make next
            while (maxHeight - totalHeight >= 256) //make structures until we're left with 256 or less units
            {
                //add a floor every time a jump is added
                if (!lastWasFloor)
                {
                    List<Box> floor = MakeFloorWithLadderEntrance(bounds, Tools.NegativeDirection(outDirection), totalHeight + 256, 208);
                    boxes.AddRange(floor);
                    lastWasFloor = true;
                }
                //make a jump
                else
                {
                    float random = Props.RandomFloat();
                    //leftover bounds contain the volume of stuff that is left to fill up
                    Bounds leftoverBounds = new Bounds(new Vector3(bounds.center.X, bounds.center.Y + totalHeight / 2, bounds.center.Z),
                        new Vector3(bounds.extents.X, bounds.extents.Y - totalHeight / 2, bounds.extents.Z));
                    //there's a slight chance that we end up with just another floor
                    if (random < 0.1f)
                    {
                        List<Box> floor = MakeFloorWithLadderEntrance(bounds, Tools.NegativeDirection(outDirection), totalHeight + 256, 192);
                        boxes.AddRange(floor);
                    }
                    //make another multi
                    else if (random < 0.4f && maxHeight - totalHeight >= Props.MaxMultiHeight)
                    {
                        List<Box> m = MakeVerticalMulti(leftoverBounds, inDirection);
                        boxes.AddRange(m);
                    }
                    //make a vertical ramp
                    else if (random < 0.7f)
                    {
                        List<Box> ramp = MakeVerticalRampJump(leftoverBounds, Tools.RandomAdjacentAlignment(outDirection), Props.random.Next(320, 384) / 2);
                        boxes.AddRange(ramp);
                    }
                    //make a sdj (will probably get rid of it in favour of two jump multis
                    else
                    {
                        List<Box> sdj = MakeStandingDoubleJump(leftoverBounds, Tools.EulerAngleForwardToDirection(outDirection), Alignment.center);
                        boxes.AddRange(sdj);
                    }
                    lastWasFloor = false;
                }
                //calculate total height of the generated structure
                totalBounds = Transform.GetStructureBounds(boxes);
                totalHeight = (int)totalBounds.extents.Y * 2;
            }

            //fill up the rest of space with a ladder...
            if (maxHeight - totalHeight > 160 || !lastWasFloor)
            {
                List<Box> ladder = MakeLadder(bounds, outDirection, 64, totalHeight + 4, Props.blockSize + (maxHeight - totalHeight - 4 >= 64 ? 64 : -6));
                boxes.AddRange(ladder);
            }
            //...or a dj
            else
            {
                Bounds leftoverBounds = new Bounds(new Vector3(bounds.center.X, bounds.center.Y + totalHeight / 2, bounds.center.Z),
                        new Vector3(bounds.extents.X, bounds.extents.Y - totalHeight / 2, bounds.extents.Z));
                Box dj = new Box(Vector3.Zero, new Vector3(16), Style.smallBlockTexture);
                AlignStructureInBounds(leftoverBounds, new List<Box> { dj }, 0, Alignment.center);
                boxes.Add(dj);
            }

            boxes.AddRange(MakeCeilingLight(bounds.maxs - new Vector3(bounds.extents.X, 0, bounds.extents.Z), 1000, false));

            return boxes;
        }

        #endregion

        #region structure generators
        //Single structure generators are placed in this region.

        /// <summary>
        /// Creates a small vertical gate which can be used to separate various map segments.
        /// </summary>
        /// <param name="bounds">Target bounds.</param>
        /// <param name="direction">Direction the gate is facing. Up and down are not valid.</param>
        /// <param name="width"></param>
        /// <param name="isMist"></param>
        /// <returns>List of boxes making the structure.</returns>
        public static List<Box> MakeGate(Bounds bounds, Direction direction, int width, bool isMist)
        {
            //Up and down directions are invalid. The structure will still be created but possibly incorrectly.
            if(direction == Direction.up || direction == Direction.down)
            {
                ConsoleExt.WarningMSG("Incorrect gate direction (" + direction.ToString() + ")");
            }

            List<Box> boxes = new List<Box>();

            int height = bounds.height;
            Contents contents = Contents.contents_detail | (isMist ? Contents.contents_mist : 0);         //contents of the boxes
            Vector3 verticalExtents = new Vector3(gateInnerThickness / 2, height / 2, gateThickness / 2); //extents of the vertical supports
            Vector3 rightPosition = new Vector3((width / 2) - gateInnerThickness / 2, 0, 0);              //right support position
            Vector3 leftPosition = new Vector3((-width / 2) + gateInnerThickness / 2, 0, 0);              //left support position

            //vertical supports
            Box rightBox = new Box(rightPosition, verticalExtents, Style.detailTexture);
            Box leftBox = new Box(leftPosition, verticalExtents, Style.detailTexture);
            rightBox.SetContents(contents);
            leftBox.SetContents(contents);
            rightBox.left.Texture = Style.ceilingTexture;
            leftBox.right.Texture = Style.ceilingTexture;

            //top horizontal support
            Vector3 topPosition = new Vector3(0, (height / 2) - gateInnerThickness / 2, 0);
            Vector3 topExtents = new Vector3((width / 2) - gateInnerThickness, gateInnerThickness / 2, gateThickness / 2);
            Box topBox = new Box(topPosition, topExtents, Style.detailTexture);
            topBox.SetContents(contents);
            topBox.bottom.Texture = Style.ceilingTexture;

            //TODO: make corners use ramps instead of standard boxes.
            //right corner
            Vector3 lb = new Vector3((width / 2) - gateInnerThickness - gateCornerSize, (height / 2) - gateInnerThickness, -gateThickness / 2);
            Vector3 lf = new Vector3((width / 2) - gateInnerThickness - gateCornerSize, (height / 2) - gateInnerThickness, gateThickness / 2);
            Vector3 rb = new Vector3((width / 2) - gateInnerThickness, (height / 2) - gateInnerThickness, -gateThickness / 2);
            Vector3 rf = new Vector3((width / 2) - gateInnerThickness, (height / 2) - gateInnerThickness, gateThickness / 2);
            Vector3 h = new Vector3(0, gateCornerSize, 0);
            Vector3 o = new Vector3(-1, 1, 1);
            Box rightCorner = new Box(rb - h, rf - h, rf - h, rb - h, lb, lf, rf, rb, Style.detailTexture);
            rightCorner.SetContents(contents);
            rightCorner.left.Texture = Style.ceilingTexture;
            rightCorner.bottom.ignorePlane = true;

            //left corner
            lb *= o; lf *= o; rb *= o; rf *= o;
            Box leftCorner = new Box(rb - h, rf - h, rf - h, rb - h, rb, rf, lf, lb, Style.detailTexture);
            leftCorner.SetContents(contents);
            leftCorner.right.Texture = Style.ceilingTexture;
            leftCorner.bottom.ignorePlane = true;

            boxes.Add(rightBox);
            boxes.Add(leftBox);
            boxes.Add(topBox);
            boxes.Add(rightCorner);
            boxes.Add(leftCorner);

            int rotation = Tools.EulerAngleForwardToDirection(direction);
            AlignStructureInBounds(bounds, boxes, rotation, Tools.DirectionToAlignment(direction));

            return boxes;
        }

        /// <summary>
        /// Creates a ceiling light at the specified position. Its volume is extending DOWN from the specified position.
        /// </summary>
        /// <param name="position">Target position.</param>
        /// <param name="strength">Standard Quake II light value.</param>
        /// <param name="makeBorders">Should the light include border brushes? This adds four more brushes per light and is not recommended for large maps.</param>
        /// <returns>List of boxes making the structure.</returns>
        public static List<Box> MakeCeilingLight(Vector3 position, int strength, bool makeBorders)
        {
            List<Box> boxes = new List<Box>();

            Vector3 center = position - new Vector3(0, ceilingLightThickness / 2, 0);
            Box light = new Box(center, new Vector3(ceilingLightSize / 2, ceilingLightThickness / 2, ceilingLightSize / 2), Style.detailTexture);

            light.SetContents(Contents.contents_detail);
            light.bottom.value = strength;
            light.bottom.surface = Surf.surf_light;
            light.bottom.Texture = Style.ceilingLightTexture;
            light.FitTexture(TextureFit.closeFit);
            boxes.Add(light);

            if (!makeBorders)
            {
                return boxes;
            }

            Box right = new Box(center + new Vector3((ceilingLightSize / 2) + ceilingLightBorder / 2, -ceilingLightBorder / 2, 0), new Vector3(ceilingLightBorder / 2,
                (ceilingLightThickness + ceilingLightBorder) / 2, (ceilingLightSize / 2) + ceilingLightBorder), Style.detailTexture);
            Box left = new Box(center - new Vector3((ceilingLightSize / 2) + ceilingLightBorder / 2, ceilingLightBorder / 2, 0), new Vector3(ceilingLightBorder / 2,
                (ceilingLightThickness + ceilingLightBorder) / 2, (ceilingLightSize / 2) + ceilingLightBorder), Style.detailTexture);
            Box front = new Box(center + new Vector3(0, -ceilingLightBorder / 2, (ceilingLightSize / 2) + ceilingLightBorder / 2),
                new Vector3((ceilingLightSize / 2), (ceilingLightThickness + ceilingLightBorder) / 2, ceilingLightBorder / 2), Style.detailTexture);
            Box back = new Box(center - new Vector3(0, ceilingLightBorder / 2, (ceilingLightSize / 2) + ceilingLightBorder / 2),
                new Vector3((ceilingLightSize / 2), (ceilingLightThickness + ceilingLightBorder) / 2, ceilingLightBorder / 2), Style.detailTexture);
            right.SetContents(Contents.contents_detail);
            left.SetContents(Contents.contents_detail);
            front.SetContents(Contents.contents_detail);
            back.SetContents(Contents.contents_detail);

            boxes.Add(right);
            boxes.Add(left);
            boxes.Add(front);
            boxes.Add(back);

            return boxes;
        }

        /// <summary>
        /// Creates a ladder and returns its boxes.
        /// </summary>
        /// <param name="bounds">Target bounds.</param>
        /// <param name="direction">Ladder will "touch the bounds" in this direction.</param>
        /// <param name="width">Total ladder width.</param>
        /// <param name="bottomOffset">Offset from bounds bottom.</param>
        /// <param name="topOffset">Offset from bounds top.</param>
        /// <param name="style">Standard style.</param>
        /// <returns>List of boxes making the structure.</returns>
        public static List<Box> MakeLadder(Bounds bounds, Direction direction, int width, int bottomOffset, int topOffset)
        {
            List<Box> boxes = new List<Box>();

            int height = (int)bounds.extents.Y * 2 - bottomOffset - topOffset; //ladder height
            Contents verticalBarContents = Contents.contents_ladder | Contents.contents_detail; //contents for vertical bars

            //calculate extents
            Vector3 verticalBarExtents = new Vector3(ladderBarThickness / 2, height / 2, ladderBarThickness / 2);
            Vector3 horizontalBarExtents = new Vector3(width / 2 - ladderBarThickness, ladderBarThickness / 2, ladderBarThickness / 2 - ladderClipThickness / 2);

            //vertical bars
            Box left = new Box(Vector3.Zero - new Vector3((width - ladderBarThickness) / 2, 0, 0), verticalBarExtents, Style.ladderSteps);
            Box right = new Box(Vector3.Zero + new Vector3((width - ladderBarThickness) / 2, 0, 0), verticalBarExtents, Style.ladderSteps);
            left.SetContents(verticalBarContents);
            right.SetContents(verticalBarContents);

            left.left.Texture = Texture.Clone(Style.ladderBars);
            right.right.Texture = Texture.Clone(Style.ladderBars);
            left.left.surface |= Surf.surf_light;
            right.right.surface |= Surf.surf_light;
            left.left.value = 1000;
            right.right.value = 1000;

            left.back.Texture = Texture.Clone(Style.ladderBars);
            right.back.Texture = Texture.Clone(Style.ladderBars);
            left.back.surface |= Surf.surf_light;
            right.back.surface |= Surf.surf_light;
            left.back.value = 1000;
            right.back.value = 1000;

            boxes.Add(left);
            boxes.Add(right);

            //horizontal bars
            Vector3 horizontalBarCenter = new Vector3(0, -verticalBarExtents.Y, verticalBarExtents.Z - (ladderBarThickness - ladderClipThickness) / 2);
            int firstCenter = (int)horizontalBarCenter.Y;
            int lastCenter = 0;
            for (int i = ladderBarThickness; i < height - ladderBarThickness; i += ladderBarThickness * 3)
            {
                Box bar = new Box(horizontalBarCenter + new Vector3(0, i, 0), horizontalBarExtents, Style.ladderSteps);
                bar.SetContents(Contents.contents_detail);
                boxes.Add(bar);

                lastCenter = i + (int)horizontalBarCenter.Y;
            }

            //clip brush
            Vector3 clipExtents = new Vector3(width / 2 - ladderBarThickness, (lastCenter - firstCenter) / 2, ladderClipThickness / 2);
            Box clip = new Box(Vector3.Zero - new Vector3(0, ladderBarThickness / 2, (ladderBarThickness - ladderClipThickness) / 2), clipExtents, Style.clip);
            clip.SetContents(Contents.contents_ladder | Contents.contents_playerclip);
            boxes.Add(clip);

            //alignment
            Vector3 newExtents = new Vector3(bounds.extents.X, bounds.extents.Y - (bottomOffset + topOffset) / 2, bounds.extents.Z);
            Vector3 mins = bounds.mins + new Vector3(0, bottomOffset, 0);
            Vector3 newCenter = newExtents + mins;
            Bounds targetBounds = new Bounds(newCenter, newExtents);

            AlignStructureInBounds(targetBounds, boxes, Tools.EulerAngleForwardToDirection(direction), Tools.DirectionToAlignment(direction));

            return boxes;
        }

        /// <summary>
        /// Creates a standing double jump followed by a ladder.
        /// </summary>
        /// <param name="bounds">Target bounds.</param>
        /// <param name="direction">Direction the jump is facing.</param>
        /// <returns>List of boxes making the structure.</returns>
        public static List<Box> MakeDJandLadder(Bounds bounds, Direction direction)
        {
            List<Box> boxes = new List<Box>();

            boxes.AddRange(MakeStandingDoubleJump(bounds, Tools.EulerAngleForwardToDirection(direction), Alignment.center));
            int height = (int)(Transform.GetStructureBounds(boxes).extents.Y * 2);
            if (bounds.height - height < 70 && height <= 256)
            {
                AlignStructureInBounds(bounds, boxes, 0, Tools.RandomAdjacentAlignment(direction));
                return boxes;
            }
            boxes.AddRange(MakeLadder(bounds, direction, 80, height + 12, -12));

            return boxes;
        }

        /// <summary>
        /// Creates a vertical multi using Props.multiHeightTables and RNG. Placement is highly constrained to ensure jumps are always doable.
        /// </summary>
        /// <param name="bounds">Target bounds. Multi is always placed at the bottom of them.</param>
        /// <param name="inDirection">Entering direction.</param>
        /// <returns>List of boxes making the structure.</returns>
        public static List<Box> MakeVerticalMulti(Bounds bounds, Direction inDirection)
        {
            List<Box> boxes = new List<Box>();
            List<Vector2> occupied = new List<Vector2>();

            int boxSize = 32;
            int[] heightTable = Props.multiHeightTables[Props.random.Next(Props.multiHeightTables.Count)];
            int currentHeight = 64;

            Axis axis = Tools.AxisFromDirection(inDirection);

            //make the dj box
            Box djBox = new Box(Vector3.Zero, new Vector3(16), Style.smallBlockTexture);
            boxes.Add(djBox);
            occupied.Add(Vector2.Zero);

            Alignment lastAlignment = Tools.RandomAdjacentAlignment(Tools.NegativeDirection(inDirection));
            Box box = BoxStandingNextToBox(djBox, new Vector3(boxSize, heightTable[0] / 2, boxSize), Style.blockTexture, lastAlignment);
            boxes.Add(box);
            occupied.Add(new Vector2(box.center.X, box.center.Z));
            Box lastbox = box;
            Box currentBox = box;

            Alignment randomAlignment = lastAlignment;
            Vector2 currentPos = new Vector2(box.center.X, box.center.Z);
            for (int i = 0; i < heightTable.Length - 1; i++)
            {
                currentHeight = heightTable[i + 1] / 2;
                while (Tools.IsBoxOverlappingStructure(currentBox, boxes) || randomAlignment == lastAlignment ||
                    randomAlignment == Alignment.backLeft || randomAlignment == Alignment.backRight ||
                    randomAlignment == Alignment.frontLeft || randomAlignment == Alignment.frontRight)
                {
                    randomAlignment = Props.RandomEnumValue<Alignment>();
                    currentBox = BoxStandingNextToBox(lastbox, new Vector3(boxSize, currentHeight, boxSize), Style.blockTexture, randomAlignment);
                    currentPos = new Vector2(lastbox.center.X, lastbox.center.Z);
                }
                lastbox = currentBox;
                occupied.Add(new Vector2(currentPos.X, currentPos.Y));
                lastAlignment = randomAlignment;
                boxes.Add(lastbox);
            }

            AlignStructureInBounds(bounds, boxes, 0, Alignment.center);

            //boxes.AddRange(MakeFloorWithLadderEntrance(bounds, Tools.NegativeDirection(outDirection), heightTable[heightTable.Length - 1] + 192, 128));

            return boxes;
        }

        /// <summary>
        /// Creates the simplest vertical ramp jump consisting of a ramp and a block to jump on.
        /// </summary>
        /// <param name="bounds">Target bounds. The jump is always placed at the bottom.</param>
        /// <param name="alignment">Standard alignment.</param>
        /// <param name="height">Specifies the height of the box. Keep in mind that the height should include ramp height.</param>
        /// <returns>List of boxes making the structure.</returns>
        public static List<Box> MakeVerticalRampJump(Bounds bounds, Alignment alignment, int height)
        {
            List<Box> boxes = new List<Box>();

            Box block = new Box(Vector3.Zero, new Vector3(32, height / 2, 32), Style.blockTexture);

            //this is very dirty and needs to be changed
            Box nextTo = BoxStandingNextToBox(block, new Vector3(32), Style.blockTexture, Alignment.back);
            Ramp ramp = new Ramp(nextTo.center, new Vector3(32), Style.detailTexture);
            boxes.Add(block);
            boxes.Add(ramp);

            AlignStructureInBounds(bounds, boxes, Tools.EulerAngleForwardToAlignment(alignment), alignment);

            return boxes;
        }

        /// <summary>
        /// Creates a floor to separate vertical structures with a ladder bottom-to-top entrance.
        /// </summary>
        /// <param name="bounds">Target bounds.</param>
        /// <param name="direction">Direction of the ladder entrance.</param>
        /// <param name="bottomOffset">Offset from bounds bottom to the floor.</param>
        /// <param name="ladderHeight">Height of the ladder. Ladder extends down from the created floor.</param>
        /// <returns>List of boxes making the structure.</returns>
        public static List<Box> MakeFloorWithLadderEntrance(Bounds bounds, Direction direction, int bottomOffset, int ladderHeight)
        {
            List<Box> boxes = new List<Box>();

            Vector3 extents = new Vector3(bounds.extents.X, 16, bounds.extents.Z);
            int hatchSize = 96;

            Vector3 floor1Center = new Vector3(0, 0, -hatchSize / 2);
            Vector3 floor1Extents = extents - new Vector3(0, 0, hatchSize / 2);
            Box floor1 = new Box(floor1Center, floor1Extents, Style.smallBlockTexture);
            int smallX = (((int)bounds.extents.X * 2) - hatchSize) / 2;
            Vector3 floor2Center = new Vector3(smallX / 2 + hatchSize / 2, 0, bounds.extents.Z - hatchSize / 2);
            Vector3 floor2Extents = new Vector3(smallX / 2, 16, hatchSize / 2);
            Box floor2 = new Box(floor2Center, floor2Extents, Style.smallBlockTexture);
            Box floor3 = new Box(floor2Center * new Vector3(-1, 1, 1), floor2Extents, Style.smallBlockTexture);
            boxes.Add(floor3);
            boxes.Add(floor2);
            boxes.Add(floor1);

            Vector3 transformOffset = bounds.mins + new Vector3(bounds.extents.X, bottomOffset, bounds.extents.Z);
            Transform.TranslateBoxes(boxes, transformOffset);
            Transform.RotateBoxesAroundPivot(boxes, Axis.y, transformOffset, Tools.EulerAngleForwardToDirection(direction));

            foreach (Box b in boxes)
            {
                b.up.Texture = Style.floorTexture;
                b.up.FitTexture(TextureFit.tile);

                b.bottom.Texture = Style.ceilingTexture;
                b.bottom.FitTexture(TextureFit.tile);

                b.left.FitTexture(TextureFit.closeFit);
                b.right.FitTexture(TextureFit.closeFit);
                b.front.FitTexture(TextureFit.closeFit);
                b.back.FitTexture(TextureFit.closeFit);
            }

            boxes.AddRange(MakeCeilingLight(transformOffset + new Vector3(0, -extents.Y, 0), 1000, false));

            boxes.AddRange(MakeLadder(bounds, direction, hatchSize - 16, bottomOffset - ladderHeight, bounds.height - bottomOffset));

            return boxes;
        }

        /// <summary>
        /// Makes a simple double jump without considering bounds height. DJ box is always centered against landing box.
        /// </summary>
        /// <param name="bounds">Target bounds.</param>
        /// <param name="rotation">Rotation angle in degrees.</param>
        /// <param name="alignment">The structure will be aligned that way. This doesn't alter rotation.</param>
        /// <param name="style">Standard style.</param>
        /// <returns>List of boxes making the structure.</returns>
        public static List<Box> MakeStandingDoubleJump(Bounds bounds, int rotation, Alignment alignment)
        {
            List<Box> boxes = new List<Box>();

            //make the "big box"
            Vector3 extents = new Vector3(32, 64, 32);
            Box landing = new Box(Vector3.Zero, extents, Style.blockTexture);

            //add 32 units on top?
            if (Props.RandomFloat() > 0.5f)
            {
                Texture s = Texture.Clone(Style.blockTexture);
                s.xScale = 0.5f;
                s.yScale = 0.5f;
                boxes.Add(new Box(new Vector3(0, extents.Y + 16, 0), new Vector3(32, 16, 32), s));
            }

            //make a dj box
            Vector3 extents2 = new Vector3(16, 16, 16);
            Vector3 center = new Vector3(0, -extents.Y + extents2.Y, -extents.Z - extents2.Z);

            Box dj = new Box(center, extents2, Style.smallBlockTexture);
            dj.FitTexture(TextureFit.stretch);
            boxes.Add(landing);
            boxes.Add(dj);

            AlignStructureInBounds(bounds, boxes, rotation, alignment);

            return boxes;
        }

        #endregion

        #region single object generators

        /// <summary>
        /// Creates a box standing next to another box. Both boxes have the same mins height (they stand on the same plane).
        /// </summary>
        /// <param name="original">Reference box which will be used to place the new box.</param>
        /// <param name="extents">Extents of the new box.</param>
        /// <param name="texture">Texture object for the new box.</param>
        /// <param name="alignment">Standard alignment for the box. Alignment.center will cause the new box to have its center inside the original box.</param>
        /// <returns>Properly world space aligned box.</returns>
        public static Box BoxStandingNextToBox(Box original, Vector3 extents, Texture texture, Alignment alignment)
        {
            Bounds ob = original.bounds;
            Vector3 center = ob.center + new Vector3(0, -ob.extents.Y + extents.Y, 0);

            switch (alignment)
            {
                case Alignment.center:
                    //ConsoleExt.WarningMSG("Incorrect box alignment!");
                    break;
                case Alignment.back:
                    center -= new Vector3(0, 0, ob.extents.Z + extents.Z);
                    break;
                case Alignment.front:
                    center += new Vector3(0, 0, ob.extents.Z + extents.Z);
                    break;
                case Alignment.left:
                    center -= new Vector3(ob.extents.X + extents.X, 0, 0);
                    break;
                case Alignment.right:
                    center += new Vector3(ob.extents.X + extents.X, 0, 0);
                    break;
                case Alignment.backLeft:
                    center -= new Vector3(ob.extents.X + extents.X, 0, ob.extents.Z + extents.Z);
                    break;
                case Alignment.frontRight:
                    center += new Vector3(ob.extents.X + extents.X, 0, ob.extents.Z + extents.Z);
                    break;
                case Alignment.backRight:
                    center -= new Vector3(-ob.extents.X - extents.X, 0, ob.extents.Z + extents.Z);
                    break;
                case Alignment.frontLeft:
                    center += new Vector3(-ob.extents.X - extents.X, 0, ob.extents.Z + extents.Z);
                    break;
            }

            Box box = new Box(center, extents, texture);

            return box;
        }

        #endregion

        #region tools

        /// <summary>
        /// Allows to align any structure made of boxes. The structure is placed at the bottom of the bounds.
        /// </summary>
        /// <param name="bounds">Target bounds to which the structure will be aligned.</param>
        /// <param name="structure">Structure to be aligned.</param>
        /// <param name="rotation">Desired rotation along Y axis (degrees).</param>
        /// <param name="alignment">Structure will be aligned to this.</param>
        public static void AlignStructureInBounds(Bounds bounds, List<Box> structure, int rotation, Alignment alignment)
        {
            Bounds structureBounds = Transform.GetStructureBounds(structure);

            //rotate each box
            foreach (Box b in structure)
            {
                Transform.RotateBoxAroundPivot(b, Axis.y, structureBounds.center, rotation);
            }

            //get bounds after rotation
            structureBounds = Transform.GetStructureBounds(structure);
            Vector3 offset = bounds.center - structureBounds.center;

            //align structure
            switch (alignment)
            {
                case Alignment.center:
                    break;
                case Alignment.back:
                    offset += new Vector3(0, 0, -bounds.extents.Z + structureBounds.extents.Z);
                    break;
                case Alignment.front:
                    offset += new Vector3(0, 0, bounds.extents.Z - structureBounds.extents.Z);
                    break;
                case Alignment.left:
                    offset += new Vector3(-bounds.extents.X + structureBounds.extents.X, 0, 0);
                    break;
                case Alignment.right:
                    offset += new Vector3(+bounds.extents.X - structureBounds.extents.X, 0, 0);
                    break;
                case Alignment.backLeft:
                    offset += new Vector3(0, 0, -bounds.extents.Z + structureBounds.extents.Z);
                    offset += new Vector3(-bounds.extents.X + structureBounds.extents.X, 0, 0);
                    break;
                case Alignment.backRight:
                    offset += new Vector3(0, 0, -bounds.extents.Z + structureBounds.extents.Z);
                    offset += new Vector3(+bounds.extents.X - structureBounds.extents.X, 0, 0);
                    break;
                case Alignment.frontLeft:
                    offset += new Vector3(0, 0, bounds.extents.Z - structureBounds.extents.Z);
                    offset += new Vector3(-bounds.extents.X + structureBounds.extents.X, 0, 0);
                    break;
                case Alignment.frontRight:
                    offset += new Vector3(0, 0, bounds.extents.Z - structureBounds.extents.Z);
                    offset += new Vector3(+bounds.extents.X - structureBounds.extents.X, 0, 0);
                    break;
            }
            //put structure "on the ground"
            offset += new Vector3(0, -bounds.extents.Y + structureBounds.extents.Y, 0);

            //translate structure to its final position
            foreach (Box b in structure)
            {
                Transform.TranslateBox(b, offset);
                b.FitTexture(TextureFit.closeFit);
            }
        }

        #endregion
    }
}
