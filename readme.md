# Q2gen

Q2gen is a Quake II jump map generator. The software is currently under development and will be getting many updates in the future. See the Planned features section for more information.

### Current features
  - Procedurally generate a layout consisting of tunnels and rooms
  - Randomly fill the map with jumps so the map can be completed from start to finish
  - Generate mapfile based on the created geometry
  - Compile the map using standalone compilers or DLLs.

Currently supported jumps:

  - SDJ
  - Multis (up to a quad jump)
  - Vertical ramps
  - Ice ramps
  - Ladders


### Planned features
  - More jump types (including horizontal jumps)
  - Map styling overhaul
  - GUI

### Compiling
The application should compile without any issues using Visual Studio 2017. Net framework 4.6.1 is required.

### Usage
Currently there is no option to pass any arguments to the app. Everything you would want to customize is either in the Props.cs or Compiler.cs. This will be more user friendly in the future, no worries.

### Making your own jump generators
*TBD*

### Contributing
Pull requests are welcome though I would recommend contacting me first before starting your own fork.